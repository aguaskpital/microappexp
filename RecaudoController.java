package com.cerounocenter.controllers;

import java.math.BigInteger;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.cerounocenter.models.RequestPagarCupon;
import com.cerounocenter.models.RequestRealizarPago;
import com.cerounocenter.models.RequestReversarPago;
import com.cerounocenter.models.ResponseConsultarContrato;
import com.cerounocenter.models.ResponseFindCuponEntidad;
import com.cerounocenter.models.ResponseGeneric;
import com.cerounocenter.models.ResponseGetCuponFecha;
import com.cerounocenter.models.ResponseGetPagoRealizado;
import com.cerounocenter.models.ResponseRealizarPago;
import com.cerounocenter.models.ResponseReversarPago;
import com.cerounocenter.models.microAppexp.Aex004EntidadModel;
import com.cerounocenter.models.microAppexp.Aex006EntidadModel;
import com.cerounocenter.models.microAppexp.AexUsuarioEntidadModel;
import com.cerounocenter.models.microAppexp.RequestMicroAppexpModel;
import com.cerounocenter.models.microAppexp.ResponseMicroAppexpModel;

@RestController
@RequestMapping("/entidadRecaudo")
public class RecaudoController extends ParentController {

	private static final long serialVersionUID = 1L;		
	
	@Value("${my-urls.ip-micro1}")
    private String ipMicro1;
	
	@Value("${my-urls.uri-valida}")
    private String uriValida;
	
	@Value("${my-urls.uri-consulta-contrato}")
    private String uriConsultaContrato;
	
	@Value("${my-urls.uri-consulta-cupon}")
    private String uriConsultaCupon;
	
	@Value("${my-urls.uri-existe-cupon}")
    private String uriExisteCupon;
	
	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("/consultarContrato")
	@ResponseBody
	public ResponseEntity<?> consultarContrato(@RequestParam String contrato, @RequestParam String usuario,
			@RequestParam String clave, HttpServletRequest request) {
		ResponseGeneric<ResponseConsultarContrato> data = new ResponseGeneric<ResponseConsultarContrato>();
		ResponseConsultarContrato rcc = new ResponseConsultarContrato();
		RequestMicroAppexpModel mam = new RequestMicroAppexpModel();
		//mam.setIp(super.getClientIpAddr(request).getHostAddress());		
		mam.setIp("190.24.11.69");
		mam.setUsuario(usuario);
		mam.setPassword(clave);
		HttpEntity<RequestMicroAppexpModel> r_mam = new HttpEntity<>(mam);
		ResponseEntity<ResponseMicroAppexpModel<AexUsuarioEntidadModel>> response = null;
		try {			
			response = restTemplate.exchange(ipMicro1.concat(uriValida),HttpMethod.POST,r_mam,new ParameterizedTypeReference<ResponseMicroAppexpModel<AexUsuarioEntidadModel>>() {});			
		} catch (HttpClientErrorException e) {			
			data.setError("ACCESO INCORRECTO");
			return new ResponseEntity<>(data, HttpStatus.UNAUTHORIZED);
		}
		if(response.getStatusCode() == HttpStatus.OK) {
			try {
				ResponseEntity<ResponseMicroAppexpModel<Aex004EntidadModel>> info_contrato = restTemplate.exchange(String.format(ipMicro1.concat(uriConsultaContrato),contrato),HttpMethod.GET,null, new ParameterizedTypeReference<ResponseMicroAppexpModel<Aex004EntidadModel>>() {});
				if(info_contrato.getStatusCode() == HttpStatus.OK) {
					System.out.println("Se recibe respuesta del microservicio >> "+info_contrato.toString());
					Aex004EntidadModel aem = info_contrato.getBody().getModel();
					System.out.println("Se muestra el modelo recibido >> "+info_contrato.getBody());
					rcc.setAtrasos(aem.getAtrasos());
					rcc.setCodigoFactura(aem.getFactcodi());
					rcc.setCodigoUsuario(aem.getSusccodi());
					rcc.setDeudaAnterior(aem.getDeudaAnterior());
					rcc.setDeudaTotal(aem.getSuscsape());
					rcc.setDireccion(aem.getDireccion());
					rcc.setEstadoConexion(aem.getEscodesc());
					rcc.setFechaLimitePago(aem.getFechaLimitePago());
					rcc.setFechaOpotunaPago(aem.getFechaOportunaPago());
					rcc.setIrregularidad(aem.getIrregularidad());
					rcc.setPagoMinimo(aem.getPagoMinimo());
					rcc.setSuscname(aem.getSuscname());
					data.setData(rcc);
					return new ResponseEntity<>(data, HttpStatus.OK);
				}else {
					return null;
				}
			} catch (Exception e) {
				data.setError(e.getMessage());
				return new ResponseEntity<>(data, HttpStatus.EXPECTATION_FAILED);
			}
		}else {
			return null;
		}
	}

	@PostMapping("/Couponentidad")
	@ResponseBody
	public ResponseEntity<?> realizarPago(@RequestBody RequestRealizarPago model) {
		ResponseEntity<ResponseMicroAppexpModel<Aex004EntidadModel>> info_contrato = null;
		ResponseEntity<ResponseMicroAppexpModel<Aex006EntidadModel>> info_cupon = null;
		ResponseEntity<ResponseMicroAppexpModel<Boolean>> existeCupon = null;
		ResponseGeneric<ResponseRealizarPago> data = new ResponseGeneric<ResponseRealizarPago>();		
		RequestMicroAppexpModel mam = new RequestMicroAppexpModel();
		String nombreSuscriptor,direccionSuscriptor;
		//mam.setIp(super.getClientIpAddr(request).getHostAddress());		
		mam.setIp("190.24.11.69");
		mam.setUsuario(model.getUsuario());
		mam.setPassword(model.getClave());
		HttpEntity<RequestMicroAppexpModel> r_mam = new HttpEntity<>(mam);
		ResponseEntity<ResponseMicroAppexpModel<AexUsuarioEntidadModel>> response = null;
		try {			
			response = restTemplate.exchange(ipMicro1.concat(uriValida),HttpMethod.POST,r_mam,new ParameterizedTypeReference<ResponseMicroAppexpModel<AexUsuarioEntidadModel>>() {});			
		} catch (HttpClientErrorException e) {			
			data.setError("ACCESO INCORRECTO");
			return new ResponseEntity<>(data, HttpStatus.UNAUTHORIZED);
		}
		if(response.getStatusCode() == HttpStatus.OK) {
			AexUsuarioEntidadModel usuarioModel = response.getBody().getModel();
			String identificadorEntidadRecaudo = usuarioModel.getUsuaenre();
			try {
				info_contrato = restTemplate.exchange(String.format(ipMicro1.concat(uriConsultaContrato),model.getContrato()),HttpMethod.GET,null, new ParameterizedTypeReference<ResponseMicroAppexpModel<Aex004EntidadModel>>() {});
			} catch (Exception e) {
				data.setError(String.format("Error, ws-info_contrato %s.", e.getMessage()));
				return new ResponseEntity<>(data, HttpStatus.EXPECTATION_FAILED);
			}
			if(info_contrato.getStatusCode() == HttpStatus.OK) {
				Aex004EntidadModel aem = info_contrato.getBody().getModel();
				BigInteger pagoMinimo = aem.getPagoMinimo();
				BigInteger deudaTotal = aem.getDeudaAnterior();
				BigInteger saldoPdte = deudaTotal.subtract(model.getValorPagar());
				System.out.println("Muestra valor model.getValorPagar() >> "+model.getValorPagar());
				System.out.println("Muestra valor pagoMinimo >> "+pagoMinimo);
				System.out.println("Muestra diff >> "+model.getValorPagar().compareTo(pagoMinimo));
				if(model.getValorPagar().compareTo(pagoMinimo) == 1) {
					try {
						info_cupon = restTemplate.exchange(String.format(ipMicro1.concat(uriConsultaCupon),model.getContrato()),HttpMethod.GET,null, new ParameterizedTypeReference<ResponseMicroAppexpModel<Aex006EntidadModel>>() {});
					} catch (Exception e) {
						data.setError(String.format("Error, ws-info_cupon %s.", e.getMessage()));
						return new ResponseEntity<>(data, HttpStatus.EXPECTATION_FAILED);
					}
					if(info_cupon.getStatusCode() == HttpStatus.OK) {
						Aex006EntidadModel aem006 = info_cupon.getBody().getModel();
						Date fechaPago = new Date();
						BigInteger saldoPendiente = aem006.getSaldoPendiente();
						BigInteger numeroCupon = aem006.getCuponNumero();
						BigInteger valorPagar = model.getValorPagar();
						nombreSuscriptor = aem006.getSuscname();
						direccionSuscriptor = aem006.getDireccion();
						BigInteger facturaContrato = aem006.getFactura();
						System.out.println("Valor resultado >> "+valorPagar.compareTo(saldoPendiente));
						System.out.println("Muestra saldoPendiente >> "+saldoPendiente);
						System.out.println("Muestra model.getValorPagar() >> "+model.getValorPagar());
						if(numeroCupon != null && (valorPagar.compareTo(saldoPendiente) == 0)) {
							BigInteger vlrReclamo = aem006.getVlrReclamo();
							BigInteger valorTotal = saldoPendiente.subtract(vlrReclamo);
							try {
								existeCupon = restTemplate.exchange(String.format(ipMicro1.concat(uriConsultaCupon),model.getContrato()),HttpMethod.GET,null, new ParameterizedTypeReference<ResponseMicroAppexpModel<Boolean>>() {});
							} catch (Exception e) {
								data.setError(String.format("Error, no existe cupon %s.", e.getMessage()));
								return new ResponseEntity<>(data, HttpStatus.EXPECTATION_FAILED);
							}
							if(!existeCupon.getBody().getModel()) {
								Map<String, Object> mapCodeBar = null;
								try {
									mapCodeBar = super.generateLineBarcodeOpendispensador(model.getContrato(), valorPagar.toString(), numeroCupon.toString());
								} catch (Exception e) {
									data.setError(String.format("Error, problema al mapear %s.", e.getMessage()));
									return new ResponseEntity<>(data, HttpStatus.EXPECTATION_FAILED);
								}
								String codigobarra = mapCodeBar.get("COD_BARRA").toString();
								ResponseRealizarPago rrp = new ResponseRealizarPago();
								rrp.setCodBarra(codigobarra);
								data.setData(rrp);
								return new ResponseEntity<>(data, HttpStatus.OK);
							}else {
								data.setError(String.format("El cupon %d ya se encuentra registrado", numeroCupon));
								return new ResponseEntity<>(data, HttpStatus.EXPECTATION_FAILED);
							}
						}else {
							generarCuponEntidad(model.getUsuario(), model.getContrato(), model.getValorPagar(),saldoPdte,nombreSuscriptor,fechaPago, facturaContrato,identificadorEntidadRecaudo);
							data.setError(String.format("Error, el numero de cupon %s o el valor a pagar %s no corresponde.", numeroCupon.toString(), valorPagar.toString()));
							return new ResponseEntity<>(data, HttpStatus.EXPECTATION_FAILED);
						}
					}else {
						data.setError(String.format("1.Responde el error %s", info_cupon.getStatusCode().getReasonPhrase()));
						return new ResponseEntity<>(data, HttpStatus.EXPECTATION_FAILED);
					}
				}else {					
					data.setError(String.format("El valor enviado a pagar %d es menor al valor minimo a pagar %d", model.getValorPagar(),pagoMinimo));
					return new ResponseEntity<>(data, HttpStatus.EXPECTATION_FAILED);
				}
			}else {
				data.setError(String.format("Codigo usuario no existe %s", model.getContrato()));
				return new ResponseEntity<>(data, HttpStatus.EXPECTATION_FAILED);
			}
		}else {
			data.setError(String.format("4.Responde el error %s", response.getStatusCode().getReasonPhrase()));
			return new ResponseEntity<>(data, HttpStatus.EXPECTATION_FAILED);
		}
	}

	private void generarCuponEntidad(String usuario, String contrato, BigInteger valorPagar, BigInteger saldoPdte,
			String nombreSuscriptor, Date fechaPago, BigInteger facturaContrato, String identificadorEntidadRecaudo) {
		// TODO Auto-generated method stub
		
	}

	@PostMapping("/ReversarPago")
	@ResponseBody
	public ResponseReversarPago reversarPago(@RequestBody RequestReversarPago model) {
		return null;
	}

	@GetMapping("/consultarCuponfecha")
	@ResponseBody
	public ResponseGetCuponFecha consultarCuponByFechas(@RequestParam String fechaini, @RequestParam String fechafin,
			@RequestParam String usuario, @RequestParam String clave) {
		return null;
	}

	@GetMapping("/consultarCuponentidad")
	@ResponseBody
	public ResponseGetPagoRealizado consultarCuponEntidad(@RequestParam Integer cupon, @RequestParam String usuario,
			@RequestParam String clave) {
		return null;
	}

	@GetMapping("/findcouponEntidad")
	@ResponseBody
	public ResponseFindCuponEntidad consultarCupon(@RequestParam Integer cupon, @RequestParam String usuario,
			@RequestParam String clave) {
		return null;
	}

	@PostMapping("/Couponentidadxcoupon")
	@ResponseBody
	public ResponseFindCuponEntidad pagarCupon(@RequestBody RequestPagarCupon model) {
		return null;
	}

}
