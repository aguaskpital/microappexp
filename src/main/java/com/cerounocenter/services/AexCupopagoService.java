package com.cerounocenter.services;

import java.util.List;

import com.cerounocenter.models.AexCupopagoModel;
import com.cerounocenter.searchFilters.AexCupopagoFilter;

public interface AexCupopagoService {

	public String insert(AexCupopagoModel model) throws Exception;	
	public List<AexCupopagoModel> findAll(AexCupopagoFilter filter) throws Exception;
	public AexCupopagoModel findAllByFilter(AexCupopagoFilter filter) throws Exception;
	
}
