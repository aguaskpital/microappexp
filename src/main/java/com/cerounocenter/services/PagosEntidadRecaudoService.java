package com.cerounocenter.services;

import com.cerounocenter.models.Aex004EntidadModel;
import com.cerounocenter.models.Aex006EntidadModel;

public interface PagosEntidadRecaudoService {

	public Aex004EntidadModel buscarContrato(Long contrato) throws Exception;
	public Aex006EntidadModel buscarCupon(Long contrato) throws Exception;
	
	public Boolean existeCupon(Long contrato, String estado) throws Exception;
	
}
