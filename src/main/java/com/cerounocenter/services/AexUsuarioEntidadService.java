package com.cerounocenter.services;

import com.cerounocenter.models.AexUsuarioEntidadModel;

public interface AexUsuarioEntidadService {

	public AexUsuarioEntidadModel validarEntidad(String user, String passwd, String ip) throws Exception;
	
}
