package com.cerounocenter.services.impl;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cerounocenter.entities.AexCupopago;
import com.cerounocenter.models.AexCupopagoModel;
import com.cerounocenter.repositories.AexCupopagoRepository;
import com.cerounocenter.searchFilters.AexCupopagoFilter;
import com.cerounocenter.services.AexCupopagoService;

@Service("aexCupopagoService")
public class AexCupopagoServiceImpl implements AexCupopagoService {
	
	@Autowired
	private AexCupopagoRepository aexCupoPagoRepo;	

	@Override
	public String insert(AexCupopagoModel model) throws Exception {
		AexCupopago entity;
		String resp;
		try {
			ModelMapper mapper = new ModelMapper();
			entity = mapper.map(model, AexCupopago.class);
		} catch (Exception e) {
			throw new Exception(e);
		}
		
		try {
			AexCupopago result = aexCupoPagoRepo.saveAndFlush(entity);
			resp = String.format("Se ha registrado el cupon numero %s ", result.getCupanume());
		} catch (Exception e) {
			throw new Exception(e);
		}
		return resp;
	}

	@Override
	public List<AexCupopagoModel> findAll(AexCupopagoFilter filter) throws Exception {
		List<AexCupopago> listCupones;
		List<AexCupopagoModel> models = new ArrayList<>();
		try {
			listCupones = aexCupoPagoRepo.findAllByCupacons(filter.getCupacons());
		} catch (Exception e) {
			throw new Exception(e);
		}
		try {
			ModelMapper mapper = new ModelMapper();
			Type listType = new TypeToken<List<AexCupopagoModel>>() {}.getType();
			models = mapper.map(listCupones, listType);
		} catch (Exception e) {
			throw new Exception(e);
		}
		return models;
	}

	@Override
	public AexCupopagoModel findAllByFilter(AexCupopagoFilter filter) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
