package com.cerounocenter.services.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cerounocenter.entities.AexUsuarioentidad;
import com.cerounocenter.models.AexUsuarioEntidadModel;
import com.cerounocenter.repositories.AexUsuarioEntidadRepository;
import com.cerounocenter.services.AexUsuarioEntidadService;

@Service("aexUsuarioEntidadService")
public class AexUsuarioEntidadServiceImpl implements AexUsuarioEntidadService {

	@Autowired
	private AexUsuarioEntidadRepository aexUsuarioEntidadRepo;
	
	@Override
	public AexUsuarioEntidadModel validarEntidad(String user, String passwd, String ip) throws Exception {
		AexUsuarioentidad aexUsuarioentidad;
		try {
			aexUsuarioentidad = aexUsuarioEntidadRepo.findByUsuausuaAndUsuapassAndUsuaestaAndUsuaipau(user, passwd, "A", ip);
		} catch (Exception e) {
			throw new Exception(e);
		}
		if(aexUsuarioentidad != null) {
			try {
				ModelMapper mapper = new ModelMapper();
				AexUsuarioEntidadModel model = mapper.map(aexUsuarioentidad, AexUsuarioEntidadModel.class);
				return model;
			} catch (Exception e) {
				throw new Exception(e);
			}
		}else {
			throw new Exception("La entidad no esta registrada o autorizada.");
		}
	}

}
