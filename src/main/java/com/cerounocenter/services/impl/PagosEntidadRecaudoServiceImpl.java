package com.cerounocenter.services.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cerounocenter.entities.Pagosentidadrecaudo;
import com.cerounocenter.models.Aex004EntidadModel;
import com.cerounocenter.models.Aex006EntidadModel;
import com.cerounocenter.repositories.PagosEntidadRecaudoRepository;
import com.cerounocenter.services.PagosEntidadRecaudoService;

@Service("pagoEntidadRecaudoService")
public class PagosEntidadRecaudoServiceImpl implements PagosEntidadRecaudoService {

	@Autowired
	private PagosEntidadRecaudoRepository pagoEntidadRecaudoRepo;

	@Override
	public Aex004EntidadModel buscarContrato(Long contrato) throws Exception {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Object[][] result = pagoEntidadRecaudoRepo.getContrato(contrato);
			if (result != null) {
				Aex004EntidadModel model = new Aex004EntidadModel();
				model.setSusccodi(new BigDecimal(result[0][0].toString()).intValue());
				model.setSuscsape(new BigInteger(result[0][1].toString()));
				model.setSuscsaan(new BigInteger(result[0][2].toString()));
				model.setFactsaan(new BigInteger(result[0][3].toString()));
				Date fechaOportunaPago = formatter.parse(result[0][4].toString());
				Date fechaLimitePago = formatter.parse(result[0][5].toString());
				model.setFechaOportunaPago(fechaOportunaPago);
				model.setFechaLimitePago(fechaLimitePago);
				model.setSuscname(result[0][6].toString());
				model.setDireccion(result[0][7].toString());
				model.setSesuesco(Integer.parseInt(result[0][8].toString()));
				model.setAtrasos(Integer.parseInt(result[0][9].toString()));
				model.setPagoMinimo(new BigInteger(result[0][10].toString()));
				model.setFactcodi(new BigInteger(result[0][11].toString()));
				model.setEscodesc(result[0][12].toString());
				model.setIrregularidad(result[0][13].toString());
				model.setFacturasPendientes(Integer.parseInt(result[0][14].toString()));
				model.setDeudaAnterior(new BigInteger(result[0][15].toString()));
				model.setVlrReclamo(new BigInteger(result[0][16].toString()));
				model.setVlrReclamoxpdo(new BigInteger(result[0][17].toString()));
				return model;
			} else {
				throw new Exception(String.format("No existe informacion para el contrato solicitado %d", contrato));
			}
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	@Override
	public Boolean existeCupon(Long contrato, String estado) throws Exception {
		List<Pagosentidadrecaudo> entities;
		try {
			entities = pagoEntidadRecaudoRepo.findAllByPaencupoAndPaenesta(contrato, estado);
		} catch (Exception e) {
			throw new Exception(e);
		}
		return entities.size() > 0;
	}

	@Override
	public Aex006EntidadModel buscarCupon(Long contrato) throws Exception {
		Object[][] result = null;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			result = pagoEntidadRecaudoRepo.getCupon(contrato);
		} catch (Exception e) {
			throw new Exception(e);
		}
		if (result != null) {			
			try {
				Aex006EntidadModel model = new Aex006EntidadModel();
				model.setContrato(new BigDecimal(result[0][0].toString()).intValue());
				model.setSaldoPendiente(new BigInteger(result[0][1].toString()));
				model.setVlrReclamo(new BigInteger(result[0][2].toString()));
				model.setSuscname(result[0][3].toString());
				model.setDireccion(result[0][4].toString());
				model.setAtrasos(Integer.parseInt(result[0][5].toString()));
				model.setFactura(new BigInteger(result[0][6].toString()));
				model.setEstadoCorte(result[0][7].toString());
				Date fechaOportunaPago = formatter.parse(result[0][8].toString());
				Date fechaLimitePago = formatter.parse(result[0][9].toString());
				model.setFechaOportunaPago(fechaOportunaPago);
				model.setFechaLimitePago(fechaLimitePago);
				model.setCuponNumero(new BigInteger(result[0][10].toString()));
				return model;
			} catch (Exception e) {
				throw new Exception(e);
			}
		} else {
			throw new Exception(String.format("No existe informacion para el cupon solicitado %d", contrato));
		}
	}

}
