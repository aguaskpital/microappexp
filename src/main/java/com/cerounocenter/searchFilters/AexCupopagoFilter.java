package com.cerounocenter.searchFilters;

import lombok.Data;

@Data
public class AexCupopagoFilter {

	private Long cupacons;
	private String cupanume;
	
}
