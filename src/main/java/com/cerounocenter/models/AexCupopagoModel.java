package com.cerounocenter.models;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

@Data
public class AexCupopagoModel {

	private Long cupacons;
	private long cupacont;
	private long cupatido;
	private String cupanume;
	private BigDecimal cupavalo;
	private long cupacaus;
	private Date cupafecr;
	private String cupaipcl;
	private String cupahocl;
	private AexUsuarioModel cupausua;
}
