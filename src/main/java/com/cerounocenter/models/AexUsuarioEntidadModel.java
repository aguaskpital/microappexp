package com.cerounocenter.models;

import java.util.Date;

import lombok.Data;

@Data
public class AexUsuarioEntidadModel {

	private Long usuacons;
    private String usuausua;
    private String usuanomb;
    private String usuapass;
    private String usuaesta;
    private Date usuafere;
    private long usuatipo;
    private String usuaenre;
    private String usuaipau;
    private Long usuaenco;
	
}
