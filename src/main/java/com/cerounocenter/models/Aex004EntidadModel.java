package com.cerounocenter.models;

import java.math.BigInteger;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class Aex004EntidadModel {

	private  Integer susccodi;
	private  BigInteger suscsape;
	private  BigInteger suscsaan;
	private  BigInteger factsaan;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private  Date fechaOportunaPago;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private  Date fechaLimitePago;
	private  String suscname;
	private  String direccion;
	private  Integer sesuesco;
	private  Integer atrasos;
	private  BigInteger pagoMinimo;
	private  BigInteger factcodi;
	private  String escodesc;
	private  String irregularidad;
	private  Integer facturasPendientes;
	private  BigInteger deudaAnterior;
	private  BigInteger vlrReclamo;
	private  BigInteger vlrReclamoxpdo;
	
}
