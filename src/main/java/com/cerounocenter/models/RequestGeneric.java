package com.cerounocenter.models;

import java.io.Serializable;

import lombok.Data;

@Data
public class RequestGeneric implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String usuario;
	private String password;
	private String ip;

}
