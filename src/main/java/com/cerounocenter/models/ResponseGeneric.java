package com.cerounocenter.models;

import java.util.Date;

import lombok.Data;

@Data
public class ResponseGeneric<T> {

	private Date fechaTx;
	private Integer codigoTx;
	private String mensaje;
	private T model;
	
}
