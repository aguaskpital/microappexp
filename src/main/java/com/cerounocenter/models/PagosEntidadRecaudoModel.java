package com.cerounocenter.models;

import java.util.Date;

import lombok.Data;

@Data
public class PagosEntidadRecaudoModel {

	private Long paencons;
    private long paencont;
    private String paenenre;
    private long paenvapa;
    private Date paenfepa;
    private String paencoba;
    private long paentipa;
    private long paencupo;
    private String paenesta;
    private long paenusua;
    private String paenipus;
	
}
