package com.cerounocenter.models;

import java.math.BigInteger;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class Aex006EntidadModel {

	private  Integer contrato;
	private  BigInteger saldoPendiente;
	private  BigInteger vlrReclamo;
	private  String suscname;
	private  String direccion;
	private  Integer atrasos;
	private  BigInteger factura;
	private  String estadoCorte;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private  Date fechaOportunaPago;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private  Date fechaLimitePago;
	private  BigInteger cuponNumero;
	
}
