package com.cerounocenter.models;

import java.util.Date;

import lombok.Data;

@Data
public class AexUsuarioModel {

	private Long usuacons;
    private String usuanomb;
    private String usuaapel;
    private String usuamail;
    private String usuapass;
    private long usuaesta;
    private Date usuafere;
    private long usuatipo;
    private Long usuaenre;
    private Date usuafeac;
    private Date usuafeui;
    private Long usuaform;
    private String usuaulip;
    private String usuatele;
    private String usuadocu;
    private String usuatido;
}
