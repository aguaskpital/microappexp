package com.cerounocenter.repositories;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cerounocenter.entities.AexUsuarioentidad;

@Repository("aexusuarioEntidadRepo")
public interface AexUsuarioEntidadRepository extends JpaRepository<AexUsuarioentidad, Serializable> {

	AexUsuarioentidad findByUsuausuaAndUsuapassAndUsuaestaAndUsuaipau(String user, String passwd, String estado, String ip);
	
}
