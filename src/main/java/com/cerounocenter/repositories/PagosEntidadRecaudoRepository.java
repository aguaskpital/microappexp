package com.cerounocenter.repositories;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cerounocenter.entities.Pagosentidadrecaudo;

@Repository("pagosEntidadRecaudoRepo")
public interface PagosEntidadRecaudoRepository extends JpaRepository<Pagosentidadrecaudo, Serializable> {

	public final String aex004 = "SELECT datos.susccodi, datos.suscsape, datos.suscsaan, datos.factsaan,"
			+ "        to_char(datos.Fecha_Oportuna_Pago, 'yyyy-MM-dd HH24:MI:ss') Fecha_Oportuna_Pago,"
			+ "        to_char(datos.Fecha_Oportuna_Pago+1, 'yyyy-MM-dd HH24:MI:ss') Fecha_Limite_Pago,"
			+ "        lbase.pkOpen.getNombre(datos.susccodi) suscname,"
			+ "        SUBSTR(lbase.pkopen.GetDireccionBarrioContrato(datos.susccodi), 1, 48) direccion,"
			+ "        nvl(lbase.pkopen.GetEstadoCorteSuscCodi(datos.susccodi),92) sesuesco,"
			+ "        lbase.pkOpen.GetAtrasosConsumos(datos.susccodi) atrasos,"
			+ "        0 pago_minimo, datos.factcodi,"
			+ "        nvl(lbase.pkopen.GetEstadoCorteSusc(datos.susccodi),'RETIRO DEFINITIVO') escodesc,"
			+ "        (select count(1) from cuencobr where cucosusc = datos.susccodi and cucoprog = 'FGCO' and cucosacu > 0) irregularidad,"
			+ "        (select count(1) from factura where factsusc = datos.susccodi and factvato <> (factvaab+factvare)) Facturas_Pendientes,"
			+ "        (select nvl(sum(factvato-factvaab-factvare),0) from factura where factsusc = datos.susccodi and factcodi <> datos.factcodi and factpefa < datos.factpefa and factvato <> (factvaab+factvare)) Deuda_Anterior,"
			+ "        datos.suscvare Vlr_Reclamo,"
			+ "        lbase.pkopen.GetVlrReclamadoxPdo(datos.susccodi,datos.factpefa) Vlr_ReclamoxPdo " + "from "
			+ "(select  susccodi, suscsape, suscsaan, suscvare, factsaan, factcodi,factpefa,"
			+ "(select max(cucofeve) from cuencobr where cucofact = factcodi) Fecha_Oportuna_Pago " + "from factura "
			+ "join suscripc on (susccodi = factsusc) " + "where   factsusc = ?1 " + "and factprog = 'FGCC' "
			+ "order by factpefa desc " + ") datos " + "where rownum = 1";
	
	public final String aex006 = "select  datos.susccodi contrato, datos.suscsape saldo_pdte, datos.suscvare Vlr_Reclamo," + 
			"lbase.pkOpen.getNombre(datos.susccodi) suscname," + 
			"SUBSTR(lbase.pkopen.GetDireccionBarrioContrato(datos.susccodi), 1, 48) direccion," + 
			"lbase.PkOpen.GetAtrasosConsumos(datos.susccodi) atrasos," + 
			"datos.factcodi factura," + 
			"lbase.pkOpen.GetEstadoCorteSusc(datos.susccodi) estacort," + 
			"to_char(datos.Fecha_Oportuna_Pago, 'yyyy-MM-dd HH24:MI:ss') Fecha_Oportuna_Pago," + 
			"to_char(datos.Fecha_Oportuna_Pago+1, 'yyyy-MM-dd HH24:MI:ss') Fecha_Limite_Pago," + 
			"case when cupovalo = datos.suscsape then cuponume else null end cuponume " + 
			"from " + 
			"(select  susccodi, suscsape, suscsaan, suscvare, factsaan, factcodi,factpefa," + 
			"(select max(cucofeve) from cuencobr where cucofact = factcodi) Fecha_Oportuna_Pago " +
			"from factura " + 
			"join suscripc on (susccodi = factsusc) " + 
			"where factsusc = ?1 " + 
			"and factprog = 'FGCC' " + 
			"order by factpefa desc " + 
			")datos " + 
			"join cupon on (cupodocu = to_char(datos.factcodi) and cupoprog = 'FIFA')" + 
			"where rownum = 1";

	@Query(value = aex004, nativeQuery = true)
	public Object[][] getContrato(Long contrato);
	
	@Query(value = aex006, nativeQuery = true)
	public Object[][] getCupon(Long cupon);
	
	public List<Pagosentidadrecaudo> findAllByPaencupoAndPaenesta(Long paenCupo, String paenEsta);

}
