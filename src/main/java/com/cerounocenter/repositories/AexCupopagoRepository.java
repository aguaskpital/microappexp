package com.cerounocenter.repositories;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cerounocenter.entities.AexCupopago;

@Repository("aexCupoPagoRepo")
public interface AexCupopagoRepository extends JpaRepository<AexCupopago, Serializable> {
	
	public List<AexCupopago> findAllByCupacons(Long id);
	
}
