/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "MAI_ENVIOS", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "MaiEnvios.findAll", query = "SELECT m FROM MaiEnvios m")})
public class MaiEnvios implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ENVICONS", nullable = false, precision = 0, scale = -127)
    private BigDecimal envicons;
    @Basic(optional = false)
    @Column(name = "ENVIFECH", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date envifech;
    @Basic(optional = false)
    @Column(name = "ENVIELIM", nullable = false)
    private BigInteger envielim;
    @Basic(optional = false)
    @Column(name = "ENVIIMPO", nullable = false)
    private BigInteger enviimpo;
    @Basic(optional = false)
    @Column(name = "ENVIREEN", nullable = false)
    private BigInteger envireen;
    @Basic(optional = false)
    @Column(name = "ENVIRESP", nullable = false)
    private BigInteger enviresp;
    @Basic(optional = false)
    @Column(name = "ENVIABAN", nullable = false)
    private BigInteger enviaban;
    @JoinColumn(name = "ENVIUSRE", referencedColumnName = "USUACONS", nullable = false)
    @ManyToOne(optional = false)
    private AexUsuario enviusre;
    @JoinColumn(name = "ENVIMENS", referencedColumnName = "MENSCONS", nullable = false)
    @ManyToOne(optional = false)
    private MaiMensajes envimens;
    @JoinColumn(name = "ENVICATE", referencedColumnName = "TBNUMERO")
    @ManyToOne
    private MaiTablas envicate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "entrenvi")
    private List<MaiEntregas> maiEntregasList;

    public MaiEnvios() {
    }

    public MaiEnvios(BigDecimal envicons) {
        this.envicons = envicons;
    }

    public MaiEnvios(BigDecimal envicons, Date envifech, BigInteger envielim, BigInteger enviimpo, BigInteger envireen, BigInteger enviresp, BigInteger enviaban) {
        this.envicons = envicons;
        this.envifech = envifech;
        this.envielim = envielim;
        this.enviimpo = enviimpo;
        this.envireen = envireen;
        this.enviresp = enviresp;
        this.enviaban = enviaban;
    }

    public BigDecimal getEnvicons() {
        return envicons;
    }

    public void setEnvicons(BigDecimal envicons) {
        this.envicons = envicons;
    }

    public Date getEnvifech() {
        return envifech;
    }

    public void setEnvifech(Date envifech) {
        this.envifech = envifech;
    }

    public BigInteger getEnvielim() {
        return envielim;
    }

    public void setEnvielim(BigInteger envielim) {
        this.envielim = envielim;
    }

    public BigInteger getEnviimpo() {
        return enviimpo;
    }

    public void setEnviimpo(BigInteger enviimpo) {
        this.enviimpo = enviimpo;
    }

    public BigInteger getEnvireen() {
        return envireen;
    }

    public void setEnvireen(BigInteger envireen) {
        this.envireen = envireen;
    }

    public BigInteger getEnviresp() {
        return enviresp;
    }

    public void setEnviresp(BigInteger enviresp) {
        this.enviresp = enviresp;
    }

    public BigInteger getEnviaban() {
        return enviaban;
    }

    public void setEnviaban(BigInteger enviaban) {
        this.enviaban = enviaban;
    }

    public AexUsuario getEnviusre() {
        return enviusre;
    }

    public void setEnviusre(AexUsuario enviusre) {
        this.enviusre = enviusre;
    }

    public MaiMensajes getEnvimens() {
        return envimens;
    }

    public void setEnvimens(MaiMensajes envimens) {
        this.envimens = envimens;
    }

    public MaiTablas getEnvicate() {
        return envicate;
    }

    public void setEnvicate(MaiTablas envicate) {
        this.envicate = envicate;
    }

    public List<MaiEntregas> getMaiEntregasList() {
        return maiEntregasList;
    }

    public void setMaiEntregasList(List<MaiEntregas> maiEntregasList) {
        this.maiEntregasList = maiEntregasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (envicons != null ? envicons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MaiEnvios)) {
            return false;
        }
        MaiEnvios other = (MaiEnvios) object;
        if ((this.envicons == null && other.envicons != null) || (this.envicons != null && !this.envicons.equals(other.envicons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.MaiEnvios[ envicons=" + envicons + " ]";
    }
    
}
