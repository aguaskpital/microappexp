/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "AEX_CLIENTE", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "AexCliente.findAll", query = "SELECT a FROM AexCliente a")})
public class AexCliente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CLIECONS", nullable = false)
    private Long cliecons;
    @Basic(optional = false)
    @Column(name = "CLIEDOID", nullable = false, length = 100)
    private String cliedoid;
    @Basic(optional = false)
    @Column(name = "CLIEESTA", nullable = false)
    private long clieesta;
    @Basic(optional = false)
    @Column(name = "CLIEFECR", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date cliefecr;
    @Column(name = "CLIEFERE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cliefere;
    @Column(name = "CLIENOMB", length = 100)
    private String clienomb;
    @Column(name = "CLIEDIRE", length = 100)
    private String cliedire;
    @Column(name = "CLIECONT")
    private Long cliecont;
    @Column(name = "CLIEMAIL", length = 10)
    private String cliemail;
    @Column(name = "CLIECICL", length = 100)
    private String cliecicl;
    @Column(name = "CLIELIQU")
    private Long clieliqu;
    @Column(name = "CLIEPEFA", length = 100)
    private String cliepefa;
    @Column(name = "CLIETIPV", length = 30)
    private String clietipv;
    @Column(name = "CLIEPERH")
    private Long clieperh;
    @Column(name = "CLIENTAN")
    private Long clientan;
    @Column(name = "CLIECAPA", length = 10)
    private String cliecapa;
    @Column(name = "CLIEUSOV", length = 30)
    private String clieusov;
    @Column(name = "CLIEACTE", length = 80)
    private String clieacte;
    @Column(name = "CLIEFEMO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cliefemo;
    @JoinColumn(name = "CLIEUSUA", referencedColumnName = "USUACONS", nullable = false)
    @ManyToOne(optional = false)
    private AexUsuario clieusua;

    public AexCliente() {
    }

    public AexCliente(Long cliecons) {
        this.cliecons = cliecons;
    }

    public AexCliente(Long cliecons, String cliedoid, long clieesta, Date cliefecr) {
        this.cliecons = cliecons;
        this.cliedoid = cliedoid;
        this.clieesta = clieesta;
        this.cliefecr = cliefecr;
    }

    public Long getCliecons() {
        return cliecons;
    }

    public void setCliecons(Long cliecons) {
        this.cliecons = cliecons;
    }

    public String getCliedoid() {
        return cliedoid;
    }

    public void setCliedoid(String cliedoid) {
        this.cliedoid = cliedoid;
    }

    public long getClieesta() {
        return clieesta;
    }

    public void setClieesta(long clieesta) {
        this.clieesta = clieesta;
    }

    public Date getCliefecr() {
        return cliefecr;
    }

    public void setCliefecr(Date cliefecr) {
        this.cliefecr = cliefecr;
    }

    public Date getCliefere() {
        return cliefere;
    }

    public void setCliefere(Date cliefere) {
        this.cliefere = cliefere;
    }

    public String getClienomb() {
        return clienomb;
    }

    public void setClienomb(String clienomb) {
        this.clienomb = clienomb;
    }

    public String getCliedire() {
        return cliedire;
    }

    public void setCliedire(String cliedire) {
        this.cliedire = cliedire;
    }

    public Long getCliecont() {
        return cliecont;
    }

    public void setCliecont(Long cliecont) {
        this.cliecont = cliecont;
    }

    public String getCliemail() {
        return cliemail;
    }

    public void setCliemail(String cliemail) {
        this.cliemail = cliemail;
    }

    public String getCliecicl() {
        return cliecicl;
    }

    public void setCliecicl(String cliecicl) {
        this.cliecicl = cliecicl;
    }

    public Long getClieliqu() {
        return clieliqu;
    }

    public void setClieliqu(Long clieliqu) {
        this.clieliqu = clieliqu;
    }

    public String getCliepefa() {
        return cliepefa;
    }

    public void setCliepefa(String cliepefa) {
        this.cliepefa = cliepefa;
    }

    public String getClietipv() {
        return clietipv;
    }

    public void setClietipv(String clietipv) {
        this.clietipv = clietipv;
    }

    public Long getClieperh() {
        return clieperh;
    }

    public void setClieperh(Long clieperh) {
        this.clieperh = clieperh;
    }

    public Long getClientan() {
        return clientan;
    }

    public void setClientan(Long clientan) {
        this.clientan = clientan;
    }

    public String getCliecapa() {
        return cliecapa;
    }

    public void setCliecapa(String cliecapa) {
        this.cliecapa = cliecapa;
    }

    public String getClieusov() {
        return clieusov;
    }

    public void setClieusov(String clieusov) {
        this.clieusov = clieusov;
    }

    public String getClieacte() {
        return clieacte;
    }

    public void setClieacte(String clieacte) {
        this.clieacte = clieacte;
    }

    public Date getCliefemo() {
        return cliefemo;
    }

    public void setCliefemo(Date cliefemo) {
        this.cliefemo = cliefemo;
    }

    public AexUsuario getClieusua() {
        return clieusua;
    }

    public void setClieusua(AexUsuario clieusua) {
        this.clieusua = clieusua;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cliecons != null ? cliecons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AexCliente)) {
            return false;
        }
        AexCliente other = (AexCliente) object;
        if ((this.cliecons == null && other.cliecons != null) || (this.cliecons != null && !this.cliecons.equals(other.cliecons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.AexCliente[ cliecons=" + cliecons + " ]";
    }
    
}
