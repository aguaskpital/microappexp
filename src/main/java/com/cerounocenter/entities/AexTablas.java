/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "AEX_TABLAS", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "AexTablas.findAll", query = "SELECT a FROM AexTablas a")})
public class AexTablas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "TBNUMERO", nullable = false)
    private Long tbnumero;
    @Column(name = "TBCODIGO", length = 20)
    private String tbcodigo;
    @Column(name = "TBCLAVE", length = 20)
    private String tbclave;
    @Column(name = "TBVALOR", length = 500)
    private String tbvalor;
    @Column(name = "TBVALINT")
    private BigInteger tbvalint;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "TBVALNUM", precision = 20, scale = 2)
    private BigDecimal tbvalnum;
    @Column(name = "TBVALFEC")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tbvalfec;

    public AexTablas() {
    }

    public AexTablas(Long tbnumero) {
        this.tbnumero = tbnumero;
    }

    public Long getTbnumero() {
        return tbnumero;
    }

    public void setTbnumero(Long tbnumero) {
        this.tbnumero = tbnumero;
    }

    public String getTbcodigo() {
        return tbcodigo;
    }

    public void setTbcodigo(String tbcodigo) {
        this.tbcodigo = tbcodigo;
    }

    public String getTbclave() {
        return tbclave;
    }

    public void setTbclave(String tbclave) {
        this.tbclave = tbclave;
    }

    public String getTbvalor() {
        return tbvalor;
    }

    public void setTbvalor(String tbvalor) {
        this.tbvalor = tbvalor;
    }

    public BigInteger getTbvalint() {
        return tbvalint;
    }

    public void setTbvalint(BigInteger tbvalint) {
        this.tbvalint = tbvalint;
    }

    public BigDecimal getTbvalnum() {
        return tbvalnum;
    }

    public void setTbvalnum(BigDecimal tbvalnum) {
        this.tbvalnum = tbvalnum;
    }

    public Date getTbvalfec() {
        return tbvalfec;
    }

    public void setTbvalfec(Date tbvalfec) {
        this.tbvalfec = tbvalfec;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tbnumero != null ? tbnumero.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AexTablas)) {
            return false;
        }
        AexTablas other = (AexTablas) object;
        if ((this.tbnumero == null && other.tbnumero != null) || (this.tbnumero != null && !this.tbnumero.equals(other.tbnumero))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.AexTablas[ tbnumero=" + tbnumero + " ]";
    }
    
}
