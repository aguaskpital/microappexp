/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "AEX_MACROMEDIDORES", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "AexMacromedidores.findAll", query = "SELECT a FROM AexMacromedidores a")})
public class AexMacromedidores implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "MACRCONS", nullable = false)
    private Long macrcons;
    @Basic(optional = false)
    @Column(name = "MACRMEDI", nullable = false)
    private long macrmedi;
    @Basic(optional = false)
    @Column(name = "MACRPULS", nullable = false)
    private long macrpuls;
    @Basic(optional = false)
    @Column(name = "MACRVOLT", nullable = false)
    private long macrvolt;
    @Column(name = "MACRHOST", length = 30)
    private String macrhost;
    @Basic(optional = false)
    @Column(name = "MACRFECR", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date macrfecr;

    public AexMacromedidores() {
    }

    public AexMacromedidores(Long macrcons) {
        this.macrcons = macrcons;
    }

    public AexMacromedidores(Long macrcons, long macrmedi, long macrpuls, long macrvolt, Date macrfecr) {
        this.macrcons = macrcons;
        this.macrmedi = macrmedi;
        this.macrpuls = macrpuls;
        this.macrvolt = macrvolt;
        this.macrfecr = macrfecr;
    }

    public Long getMacrcons() {
        return macrcons;
    }

    public void setMacrcons(Long macrcons) {
        this.macrcons = macrcons;
    }

    public long getMacrmedi() {
        return macrmedi;
    }

    public void setMacrmedi(long macrmedi) {
        this.macrmedi = macrmedi;
    }

    public long getMacrpuls() {
        return macrpuls;
    }

    public void setMacrpuls(long macrpuls) {
        this.macrpuls = macrpuls;
    }

    public long getMacrvolt() {
        return macrvolt;
    }

    public void setMacrvolt(long macrvolt) {
        this.macrvolt = macrvolt;
    }

    public String getMacrhost() {
        return macrhost;
    }

    public void setMacrhost(String macrhost) {
        this.macrhost = macrhost;
    }

    public Date getMacrfecr() {
        return macrfecr;
    }

    public void setMacrfecr(Date macrfecr) {
        this.macrfecr = macrfecr;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (macrcons != null ? macrcons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AexMacromedidores)) {
            return false;
        }
        AexMacromedidores other = (AexMacromedidores) object;
        if ((this.macrcons == null && other.macrcons != null) || (this.macrcons != null && !this.macrcons.equals(other.macrcons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.AexMacromedidores[ macrcons=" + macrcons + " ]";
    }
    
}
