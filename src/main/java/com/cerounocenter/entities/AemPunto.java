/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "AEM_PUNTO", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "AemPunto.findAll", query = "SELECT a FROM AemPunto a")})
public class AemPunto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PUNTCONS", nullable = false)
    private Long puntcons;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "PUNTLATI", precision = 20, scale = 15)
    private BigDecimal puntlati;
    @Column(name = "PUNTLONG", precision = 20, scale = 15)
    private BigDecimal puntlong;
    @Column(name = "PUNTINFO", length = 1000)
    private String puntinfo;
    @Column(name = "PUNTNOMB", length = 50)
    private String puntnomb;
    @Column(name = "PUNTTIPO", length = 10)
    private String punttipo;
    @Column(name = "PUNTDIRE", length = 100)
    private String puntdire;
    @Column(name = "PUNTUSCR", length = 100)
    private String puntuscr;
    @Column(name = "PUNTFECR")
    @Temporal(TemporalType.TIMESTAMP)
    private Date puntfecr;
    @Column(name = "PUNTESTA", length = 20)
    private String puntesta;

    public AemPunto() {
    }

    public AemPunto(Long puntcons) {
        this.puntcons = puntcons;
    }

    public Long getPuntcons() {
        return puntcons;
    }

    public void setPuntcons(Long puntcons) {
        this.puntcons = puntcons;
    }

    public BigDecimal getPuntlati() {
        return puntlati;
    }

    public void setPuntlati(BigDecimal puntlati) {
        this.puntlati = puntlati;
    }

    public BigDecimal getPuntlong() {
        return puntlong;
    }

    public void setPuntlong(BigDecimal puntlong) {
        this.puntlong = puntlong;
    }

    public String getPuntinfo() {
        return puntinfo;
    }

    public void setPuntinfo(String puntinfo) {
        this.puntinfo = puntinfo;
    }

    public String getPuntnomb() {
        return puntnomb;
    }

    public void setPuntnomb(String puntnomb) {
        this.puntnomb = puntnomb;
    }

    public String getPunttipo() {
        return punttipo;
    }

    public void setPunttipo(String punttipo) {
        this.punttipo = punttipo;
    }

    public String getPuntdire() {
        return puntdire;
    }

    public void setPuntdire(String puntdire) {
        this.puntdire = puntdire;
    }

    public String getPuntuscr() {
        return puntuscr;
    }

    public void setPuntuscr(String puntuscr) {
        this.puntuscr = puntuscr;
    }

    public Date getPuntfecr() {
        return puntfecr;
    }

    public void setPuntfecr(Date puntfecr) {
        this.puntfecr = puntfecr;
    }

    public String getPuntesta() {
        return puntesta;
    }

    public void setPuntesta(String puntesta) {
        this.puntesta = puntesta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (puntcons != null ? puntcons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AemPunto)) {
            return false;
        }
        AemPunto other = (AemPunto) object;
        if ((this.puntcons == null && other.puntcons != null) || (this.puntcons != null && !this.puntcons.equals(other.puntcons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.AemPunto[ puntcons=" + puntcons + " ]";
    }
    
}
