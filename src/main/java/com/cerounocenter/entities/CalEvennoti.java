/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "CAL_EVENNOTI", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "CalEvennoti.findAll", query = "SELECT c FROM CalEvennoti c")})
public class CalEvennoti implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "EVNOCONS", nullable = false)
    private Long evnocons;
    @Basic(optional = false)
    @Column(name = "EVNOEVEN", nullable = false)
    private long evnoeven;
    @Basic(optional = false)
    @Column(name = "EVNOFECH", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date evnofech;
    @Column(name = "EVNOTIPO")
    private Long evnotipo;
    @Column(name = "EVNOALER")
    private Long evnoaler;
    @Column(name = "EVNOESTA")
    private Short evnoesta;
    @Column(name = "EVNODESC", length = 100)
    private String evnodesc;
    @Basic(optional = false)
    @Column(name = "EVNOUSCR", nullable = false)
    private long evnouscr;
    @Basic(optional = false)
    @Column(name = "EVNOFECR", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date evnofecr;

    public CalEvennoti() {
    }

    public CalEvennoti(Long evnocons) {
        this.evnocons = evnocons;
    }

    public CalEvennoti(Long evnocons, long evnoeven, Date evnofech, long evnouscr, Date evnofecr) {
        this.evnocons = evnocons;
        this.evnoeven = evnoeven;
        this.evnofech = evnofech;
        this.evnouscr = evnouscr;
        this.evnofecr = evnofecr;
    }

    public Long getEvnocons() {
        return evnocons;
    }

    public void setEvnocons(Long evnocons) {
        this.evnocons = evnocons;
    }

    public long getEvnoeven() {
        return evnoeven;
    }

    public void setEvnoeven(long evnoeven) {
        this.evnoeven = evnoeven;
    }

    public Date getEvnofech() {
        return evnofech;
    }

    public void setEvnofech(Date evnofech) {
        this.evnofech = evnofech;
    }

    public Long getEvnotipo() {
        return evnotipo;
    }

    public void setEvnotipo(Long evnotipo) {
        this.evnotipo = evnotipo;
    }

    public Long getEvnoaler() {
        return evnoaler;
    }

    public void setEvnoaler(Long evnoaler) {
        this.evnoaler = evnoaler;
    }

    public Short getEvnoesta() {
        return evnoesta;
    }

    public void setEvnoesta(Short evnoesta) {
        this.evnoesta = evnoesta;
    }

    public String getEvnodesc() {
        return evnodesc;
    }

    public void setEvnodesc(String evnodesc) {
        this.evnodesc = evnodesc;
    }

    public long getEvnouscr() {
        return evnouscr;
    }

    public void setEvnouscr(long evnouscr) {
        this.evnouscr = evnouscr;
    }

    public Date getEvnofecr() {
        return evnofecr;
    }

    public void setEvnofecr(Date evnofecr) {
        this.evnofecr = evnofecr;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (evnocons != null ? evnocons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CalEvennoti)) {
            return false;
        }
        CalEvennoti other = (CalEvennoti) object;
        if ((this.evnocons == null && other.evnocons != null) || (this.evnocons != null && !this.evnocons.equals(other.evnocons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.CalEvennoti[ evnocons=" + evnocons + " ]";
    }
    
}
