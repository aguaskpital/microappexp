/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "AEX_CUPOPAGO", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "AexCupopago.findAll", query = "SELECT a FROM AexCupopago a")})
public class AexCupopago implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CUPACONS", nullable = false)
    private Long cupacons;
    @Basic(optional = false)
    @Column(name = "CUPACONT", nullable = false)
    private long cupacont;
    @Basic(optional = false)
    @Column(name = "CUPATIDO", nullable = false)
    private long cupatido;
    @Basic(optional = false)
    @Column(name = "CUPANUME", nullable = false, length = 10)
    private String cupanume;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "CUPAVALO", nullable = false, precision = 20, scale = 2)
    private BigDecimal cupavalo;
    @Basic(optional = false)
    @Column(name = "CUPACAUS", nullable = false)
    private long cupacaus;
    @Basic(optional = false)
    @Column(name = "CUPAFECR", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date cupafecr;
    @Basic(optional = false)
    @Column(name = "CUPAIPCL", nullable = false, length = 20)
    private String cupaipcl;
    @Basic(optional = false)
    @Column(name = "CUPAHOCL", nullable = false, length = 20)
    private String cupahocl;
    @JoinColumn(name = "CUPAUSUA", referencedColumnName = "USUACONS", nullable = false)
    @ManyToOne(optional = false)
    private AexUsuario cupausua;

    public AexCupopago() {
    }

    public AexCupopago(Long cupacons) {
        this.cupacons = cupacons;
    }

    public AexCupopago(Long cupacons, long cupacont, long cupatido, String cupanume, BigDecimal cupavalo, long cupacaus, Date cupafecr, String cupaipcl, String cupahocl) {
        this.cupacons = cupacons;
        this.cupacont = cupacont;
        this.cupatido = cupatido;
        this.cupanume = cupanume;
        this.cupavalo = cupavalo;
        this.cupacaus = cupacaus;
        this.cupafecr = cupafecr;
        this.cupaipcl = cupaipcl;
        this.cupahocl = cupahocl;
    }

    public Long getCupacons() {
        return cupacons;
    }

    public void setCupacons(Long cupacons) {
        this.cupacons = cupacons;
    }

    public long getCupacont() {
        return cupacont;
    }

    public void setCupacont(long cupacont) {
        this.cupacont = cupacont;
    }

    public long getCupatido() {
        return cupatido;
    }

    public void setCupatido(long cupatido) {
        this.cupatido = cupatido;
    }

    public String getCupanume() {
        return cupanume;
    }

    public void setCupanume(String cupanume) {
        this.cupanume = cupanume;
    }

    public BigDecimal getCupavalo() {
        return cupavalo;
    }

    public void setCupavalo(BigDecimal cupavalo) {
        this.cupavalo = cupavalo;
    }

    public long getCupacaus() {
        return cupacaus;
    }

    public void setCupacaus(long cupacaus) {
        this.cupacaus = cupacaus;
    }

    public Date getCupafecr() {
        return cupafecr;
    }

    public void setCupafecr(Date cupafecr) {
        this.cupafecr = cupafecr;
    }

    public String getCupaipcl() {
        return cupaipcl;
    }

    public void setCupaipcl(String cupaipcl) {
        this.cupaipcl = cupaipcl;
    }

    public String getCupahocl() {
        return cupahocl;
    }

    public void setCupahocl(String cupahocl) {
        this.cupahocl = cupahocl;
    }

    public AexUsuario getCupausua() {
        return cupausua;
    }

    public void setCupausua(AexUsuario cupausua) {
        this.cupausua = cupausua;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cupacons != null ? cupacons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AexCupopago)) {
            return false;
        }
        AexCupopago other = (AexCupopago) object;
        if ((this.cupacons == null && other.cupacons != null) || (this.cupacons != null && !this.cupacons.equals(other.cupacons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.AexCupopago[ cupacons=" + cupacons + " ]";
    }
    
}
