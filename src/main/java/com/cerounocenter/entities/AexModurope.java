/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "AEX_MODUROPE", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "AexModurope.findAll", query = "SELECT a FROM AexModurope a")})
public class AexModurope implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "MOROCONS", nullable = false, precision = 22, scale = 10)
    private BigDecimal morocons;
    @JoinColumn(name = "MOROMODU", referencedColumnName = "MODUCONS", nullable = false)
    @ManyToOne(optional = false)
    private AexModulo moromodu;
    @JoinColumn(name = "MOROROPE", referencedColumnName = "ROPECONS", nullable = false)
    @ManyToOne(optional = false)
    private AexRoleperm mororope;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usromoro")
    private List<AexUserrole> aexUserroleList;

    public AexModurope() {
    }

    public AexModurope(BigDecimal morocons) {
        this.morocons = morocons;
    }

    public BigDecimal getMorocons() {
        return morocons;
    }

    public void setMorocons(BigDecimal morocons) {
        this.morocons = morocons;
    }

    public AexModulo getMoromodu() {
        return moromodu;
    }

    public void setMoromodu(AexModulo moromodu) {
        this.moromodu = moromodu;
    }

    public AexRoleperm getMororope() {
        return mororope;
    }

    public void setMororope(AexRoleperm mororope) {
        this.mororope = mororope;
    }

    public List<AexUserrole> getAexUserroleList() {
        return aexUserroleList;
    }

    public void setAexUserroleList(List<AexUserrole> aexUserroleList) {
        this.aexUserroleList = aexUserroleList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (morocons != null ? morocons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AexModurope)) {
            return false;
        }
        AexModurope other = (AexModurope) object;
        if ((this.morocons == null && other.morocons != null) || (this.morocons != null && !this.morocons.equals(other.morocons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.AexModurope[ morocons=" + morocons + " ]";
    }
    
}
