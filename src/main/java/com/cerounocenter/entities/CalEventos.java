/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "CAL_EVENTOS", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "CalEventos.findAll", query = "SELECT c FROM CalEventos c")})
public class CalEventos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "EVENCONS", nullable = false)
    private Long evencons;
    @Column(name = "EVENDESC", length = 100)
    private String evendesc;
    @Basic(optional = false)
    @Column(name = "EVENFEIN", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date evenfein;
    @Basic(optional = false)
    @Column(name = "EVENFEFI", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date evenfefi;
    @Column(name = "EVENIMPO")
    private Long evenimpo;
    @Column(name = "EVENPERI")
    private Long evenperi;
    @Column(name = "EVENICON")
    private Long evenicon;
    @Basic(optional = false)
    @Column(name = "EVENUSCR", nullable = false)
    private long evenuscr;
    @Basic(optional = false)
    @Column(name = "EVENFECR", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date evenfecr;
    @Column(name = "EVENESTA")
    private Short evenesta;
    @Column(name = "EVENVIBU", length = 2)
    private String evenvibu;
    @Column(name = "EVENFFIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date evenffin;
    @Column(name = "EVENTIPO")
    private Short eventipo;

    public CalEventos() {
    }

    public CalEventos(Long evencons) {
        this.evencons = evencons;
    }

    public CalEventos(Long evencons, Date evenfein, Date evenfefi, long evenuscr, Date evenfecr) {
        this.evencons = evencons;
        this.evenfein = evenfein;
        this.evenfefi = evenfefi;
        this.evenuscr = evenuscr;
        this.evenfecr = evenfecr;
    }

    public Long getEvencons() {
        return evencons;
    }

    public void setEvencons(Long evencons) {
        this.evencons = evencons;
    }

    public String getEvendesc() {
        return evendesc;
    }

    public void setEvendesc(String evendesc) {
        this.evendesc = evendesc;
    }

    public Date getEvenfein() {
        return evenfein;
    }

    public void setEvenfein(Date evenfein) {
        this.evenfein = evenfein;
    }

    public Date getEvenfefi() {
        return evenfefi;
    }

    public void setEvenfefi(Date evenfefi) {
        this.evenfefi = evenfefi;
    }

    public Long getEvenimpo() {
        return evenimpo;
    }

    public void setEvenimpo(Long evenimpo) {
        this.evenimpo = evenimpo;
    }

    public Long getEvenperi() {
        return evenperi;
    }

    public void setEvenperi(Long evenperi) {
        this.evenperi = evenperi;
    }

    public Long getEvenicon() {
        return evenicon;
    }

    public void setEvenicon(Long evenicon) {
        this.evenicon = evenicon;
    }

    public long getEvenuscr() {
        return evenuscr;
    }

    public void setEvenuscr(long evenuscr) {
        this.evenuscr = evenuscr;
    }

    public Date getEvenfecr() {
        return evenfecr;
    }

    public void setEvenfecr(Date evenfecr) {
        this.evenfecr = evenfecr;
    }

    public Short getEvenesta() {
        return evenesta;
    }

    public void setEvenesta(Short evenesta) {
        this.evenesta = evenesta;
    }

    public String getEvenvibu() {
        return evenvibu;
    }

    public void setEvenvibu(String evenvibu) {
        this.evenvibu = evenvibu;
    }

    public Date getEvenffin() {
        return evenffin;
    }

    public void setEvenffin(Date evenffin) {
        this.evenffin = evenffin;
    }

    public Short getEventipo() {
        return eventipo;
    }

    public void setEventipo(Short eventipo) {
        this.eventipo = eventipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (evencons != null ? evencons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CalEventos)) {
            return false;
        }
        CalEventos other = (CalEventos) object;
        if ((this.evencons == null && other.evencons != null) || (this.evencons != null && !this.evencons.equals(other.evencons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.CalEventos[ evencons=" + evencons + " ]";
    }
    
}
