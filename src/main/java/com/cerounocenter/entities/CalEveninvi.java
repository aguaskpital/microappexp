/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "CAL_EVENINVI", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "CalEveninvi.findAll", query = "SELECT c FROM CalEveninvi c")})
public class CalEveninvi implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "EVINCONS", nullable = false)
    private Long evincons;
    @Basic(optional = false)
    @Column(name = "EVINEVEN", nullable = false)
    private long evineven;
    @Basic(optional = false)
    @Column(name = "EVINUSUA", nullable = false)
    private long evinusua;
    @Column(name = "EVINNOMB", length = 100)
    private String evinnomb;
    @Column(name = "EVINESTA")
    private Short evinesta;

    public CalEveninvi() {
    }

    public CalEveninvi(Long evincons) {
        this.evincons = evincons;
    }

    public CalEveninvi(Long evincons, long evineven, long evinusua) {
        this.evincons = evincons;
        this.evineven = evineven;
        this.evinusua = evinusua;
    }

    public Long getEvincons() {
        return evincons;
    }

    public void setEvincons(Long evincons) {
        this.evincons = evincons;
    }

    public long getEvineven() {
        return evineven;
    }

    public void setEvineven(long evineven) {
        this.evineven = evineven;
    }

    public long getEvinusua() {
        return evinusua;
    }

    public void setEvinusua(long evinusua) {
        this.evinusua = evinusua;
    }

    public String getEvinnomb() {
        return evinnomb;
    }

    public void setEvinnomb(String evinnomb) {
        this.evinnomb = evinnomb;
    }

    public Short getEvinesta() {
        return evinesta;
    }

    public void setEvinesta(Short evinesta) {
        this.evinesta = evinesta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (evincons != null ? evincons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CalEveninvi)) {
            return false;
        }
        CalEveninvi other = (CalEveninvi) object;
        if ((this.evincons == null && other.evincons != null) || (this.evincons != null && !this.evincons.equals(other.evincons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.CalEveninvi[ evincons=" + evincons + " ]";
    }
    
}
