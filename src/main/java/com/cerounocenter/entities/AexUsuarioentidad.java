package com.cerounocenter.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author CEROUNOCENTER S.A.S
 */
@Entity
@Table(name = "AEX_USUARIOENTIDAD", catalog = "", schema = "APPEXP")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AexUsuarioentidad.findAll", query = "SELECT a FROM AexUsuarioentidad a")
    , @NamedQuery(name = "AexUsuarioentidad.findByUsuacons", query = "SELECT a FROM AexUsuarioentidad a WHERE a.usuacons = :usuacons")
    , @NamedQuery(name = "AexUsuarioentidad.findByUsuausua", query = "SELECT a FROM AexUsuarioentidad a WHERE a.usuausua = :usuausua")
    , @NamedQuery(name = "AexUsuarioentidad.findByUsuanomb", query = "SELECT a FROM AexUsuarioentidad a WHERE a.usuanomb = :usuanomb")
    , @NamedQuery(name = "AexUsuarioentidad.findByUsuapass", query = "SELECT a FROM AexUsuarioentidad a WHERE a.usuapass = :usuapass")
    , @NamedQuery(name = "AexUsuarioentidad.findByUsuaesta", query = "SELECT a FROM AexUsuarioentidad a WHERE a.usuaesta = :usuaesta")
    , @NamedQuery(name = "AexUsuarioentidad.findByUsuafere", query = "SELECT a FROM AexUsuarioentidad a WHERE a.usuafere = :usuafere")
    , @NamedQuery(name = "AexUsuarioentidad.findByUsuatipo", query = "SELECT a FROM AexUsuarioentidad a WHERE a.usuatipo = :usuatipo")
    , @NamedQuery(name = "AexUsuarioentidad.findByUsuaenre", query = "SELECT a FROM AexUsuarioentidad a WHERE a.usuaenre = :usuaenre")
    , @NamedQuery(name = "AexUsuarioentidad.findByUsuaipau", query = "SELECT a FROM AexUsuarioentidad a WHERE a.usuaipau = :usuaipau")
    , @NamedQuery(name = "AexUsuarioentidad.findByUsuaenco", query = "SELECT a FROM AexUsuarioentidad a WHERE a.usuaenco = :usuaenco")})
public class AexUsuarioentidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "USUACONS", nullable = false)
    private Long usuacons;
    @Basic(optional = false)
    @Column(name = "USUAUSUA", nullable = false, length = 30)
    private String usuausua;
    @Basic(optional = false)
    @Column(name = "USUANOMB", nullable = false, length = 100)
    private String usuanomb;
    @Basic(optional = false)
    @Column(name = "USUAPASS", nullable = false, length = 100)
    private String usuapass;
    @Basic(optional = false)
    @Column(name = "USUAESTA", nullable = false, length = 5)
    private String usuaesta;
    @Basic(optional = false)
    @Column(name = "USUAFERE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date usuafere;
    @Basic(optional = false)
    @Column(name = "USUATIPO", nullable = false)
    private long usuatipo;
    @Basic(optional = false)
    @Column(name = "USUAENRE", nullable = false, length = 30)
    private String usuaenre;
    @Column(name = "USUAIPAU", length = 20)
    private String usuaipau;
    @Column(name = "USUAENCO")
    private Long usuaenco;

    public AexUsuarioentidad() {
    }

    public AexUsuarioentidad(Long usuacons) {
        this.usuacons = usuacons;
    }

    public AexUsuarioentidad(Long usuacons, String usuausua, String usuanomb, String usuapass, String usuaesta, Date usuafere, long usuatipo, String usuaenre) {
        this.usuacons = usuacons;
        this.usuausua = usuausua;
        this.usuanomb = usuanomb;
        this.usuapass = usuapass;
        this.usuaesta = usuaesta;
        this.usuafere = usuafere;
        this.usuatipo = usuatipo;
        this.usuaenre = usuaenre;
    }

    public Long getUsuacons() {
        return usuacons;
    }

    public void setUsuacons(Long usuacons) {
        this.usuacons = usuacons;
    }

    public String getUsuausua() {
        return usuausua;
    }

    public void setUsuausua(String usuausua) {
        this.usuausua = usuausua;
    }

    public String getUsuanomb() {
        return usuanomb;
    }

    public void setUsuanomb(String usuanomb) {
        this.usuanomb = usuanomb;
    }

    public String getUsuapass() {
        return usuapass;
    }

    public void setUsuapass(String usuapass) {
        this.usuapass = usuapass;
    }

    public String getUsuaesta() {
        return usuaesta;
    }

    public void setUsuaesta(String usuaesta) {
        this.usuaesta = usuaesta;
    }

    public Date getUsuafere() {
        return usuafere;
    }

    public void setUsuafere(Date usuafere) {
        this.usuafere = usuafere;
    }

    public long getUsuatipo() {
        return usuatipo;
    }

    public void setUsuatipo(long usuatipo) {
        this.usuatipo = usuatipo;
    }

    public String getUsuaenre() {
        return usuaenre;
    }

    public void setUsuaenre(String usuaenre) {
        this.usuaenre = usuaenre;
    }

    public String getUsuaipau() {
        return usuaipau;
    }

    public void setUsuaipau(String usuaipau) {
        this.usuaipau = usuaipau;
    }

    public Long getUsuaenco() {
        return usuaenco;
    }

    public void setUsuaenco(Long usuaenco) {
        this.usuaenco = usuaenco;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuacons != null ? usuacons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AexUsuarioentidad)) {
            return false;
        }
        AexUsuarioentidad other = (AexUsuarioentidad) object;
        if ((this.usuacons == null && other.usuacons != null) || (this.usuacons != null && !this.usuacons.equals(other.usuacons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.AexUsuarioentidad[ usuacons=" + usuacons + " ]";
    }
    
}
