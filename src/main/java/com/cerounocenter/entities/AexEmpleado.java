/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "AEX_EMPLEADO", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "AexEmpleado.findAll", query = "SELECT a FROM AexEmpleado a")})
public class AexEmpleado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "EMPLUSUA", nullable = false)
    private Long emplusua;
    @Column(name = "EMPLTIDO")
    private Long empltido;
    @Column(name = "EMPLDOID")
    private Long empldoid;
    @Column(name = "EMPLUSDO", length = 50)
    private String emplusdo;

    public AexEmpleado() {
    }

    public AexEmpleado(Long emplusua) {
        this.emplusua = emplusua;
    }

    public Long getEmplusua() {
        return emplusua;
    }

    public void setEmplusua(Long emplusua) {
        this.emplusua = emplusua;
    }

    public Long getEmpltido() {
        return empltido;
    }

    public void setEmpltido(Long empltido) {
        this.empltido = empltido;
    }

    public Long getEmpldoid() {
        return empldoid;
    }

    public void setEmpldoid(Long empldoid) {
        this.empldoid = empldoid;
    }

    public String getEmplusdo() {
        return emplusdo;
    }

    public void setEmplusdo(String emplusdo) {
        this.emplusdo = emplusdo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (emplusua != null ? emplusua.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AexEmpleado)) {
            return false;
        }
        AexEmpleado other = (AexEmpleado) object;
        if ((this.emplusua == null && other.emplusua != null) || (this.emplusua != null && !this.emplusua.equals(other.emplusua))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.AexEmpleado[ emplusua=" + emplusua + " ]";
    }
    
}
