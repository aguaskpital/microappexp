/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "POL_EMPRESA", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "PolEmpresa.findAll", query = "SELECT p FROM PolEmpresa p")})
public class PolEmpresa implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "EMPRCONS", nullable = false, precision = 22, scale = 10)
    private BigDecimal emprcons;
    @Column(name = "EMPRNOMB", length = 50)
    private String emprnomb;
    @Column(name = "EMPRNIT", length = 50)
    private String emprnit;
    @Column(name = "EMPRIDCO", length = 50)
    private String empridco;
    @Column(name = "EMPRIDAC", length = 50)
    private String empridac;
    @Column(name = "EMPRTECO", length = 50)
    private String emprteco;
    @Column(name = "EMPRCOUN", length = 50)
    private String emprcoun;
    @OneToMany(mappedBy = "tranempr")
    private List<PolTransaccion> polTransaccionList;

    public PolEmpresa() {
    }

    public PolEmpresa(BigDecimal emprcons) {
        this.emprcons = emprcons;
    }

    public BigDecimal getEmprcons() {
        return emprcons;
    }

    public void setEmprcons(BigDecimal emprcons) {
        this.emprcons = emprcons;
    }

    public String getEmprnomb() {
        return emprnomb;
    }

    public void setEmprnomb(String emprnomb) {
        this.emprnomb = emprnomb;
    }

    public String getEmprnit() {
        return emprnit;
    }

    public void setEmprnit(String emprnit) {
        this.emprnit = emprnit;
    }

    public String getEmpridco() {
        return empridco;
    }

    public void setEmpridco(String empridco) {
        this.empridco = empridco;
    }

    public String getEmpridac() {
        return empridac;
    }

    public void setEmpridac(String empridac) {
        this.empridac = empridac;
    }

    public String getEmprteco() {
        return emprteco;
    }

    public void setEmprteco(String emprteco) {
        this.emprteco = emprteco;
    }

    public String getEmprcoun() {
        return emprcoun;
    }

    public void setEmprcoun(String emprcoun) {
        this.emprcoun = emprcoun;
    }

    public List<PolTransaccion> getPolTransaccionList() {
        return polTransaccionList;
    }

    public void setPolTransaccionList(List<PolTransaccion> polTransaccionList) {
        this.polTransaccionList = polTransaccionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (emprcons != null ? emprcons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PolEmpresa)) {
            return false;
        }
        PolEmpresa other = (PolEmpresa) object;
        if ((this.emprcons == null && other.emprcons != null) || (this.emprcons != null && !this.emprcons.equals(other.emprcons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.PolEmpresa[ emprcons=" + emprcons + " ]";
    }
    
}
