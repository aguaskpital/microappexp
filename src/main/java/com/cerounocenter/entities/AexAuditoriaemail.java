/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "AEX_AUDITORIAEMAIL", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "AexAuditoriaemail.findAll", query = "SELECT a FROM AexAuditoriaemail a")})
public class AexAuditoriaemail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "AUDICONS", nullable = false)
    private Long audicons;
    @Column(name = "AUDIMAIL", length = 100)
    private String audimail;
    @Column(name = "AUDISUSC", length = 4000)
    private String audisusc;
    @Basic(optional = false)
    @Column(name = "AUDIFECR", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date audifecr;
    @Column(name = "AUDICONF", length = 10)
    private String audiconf;
    @Column(name = "AUDIPEFA", length = 100)
    private String audipefa;

    public AexAuditoriaemail() {
    }

    public AexAuditoriaemail(Long audicons) {
        this.audicons = audicons;
    }

    public AexAuditoriaemail(Long audicons, Date audifecr) {
        this.audicons = audicons;
        this.audifecr = audifecr;
    }

    public Long getAudicons() {
        return audicons;
    }

    public void setAudicons(Long audicons) {
        this.audicons = audicons;
    }

    public String getAudimail() {
        return audimail;
    }

    public void setAudimail(String audimail) {
        this.audimail = audimail;
    }

    public String getAudisusc() {
        return audisusc;
    }

    public void setAudisusc(String audisusc) {
        this.audisusc = audisusc;
    }

    public Date getAudifecr() {
        return audifecr;
    }

    public void setAudifecr(Date audifecr) {
        this.audifecr = audifecr;
    }

    public String getAudiconf() {
        return audiconf;
    }

    public void setAudiconf(String audiconf) {
        this.audiconf = audiconf;
    }

    public String getAudipefa() {
        return audipefa;
    }

    public void setAudipefa(String audipefa) {
        this.audipefa = audipefa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (audicons != null ? audicons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AexAuditoriaemail)) {
            return false;
        }
        AexAuditoriaemail other = (AexAuditoriaemail) object;
        if ((this.audicons == null && other.audicons != null) || (this.audicons != null && !this.audicons.equals(other.audicons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.AexAuditoriaemail[ audicons=" + audicons + " ]";
    }
    
}
