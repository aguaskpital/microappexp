/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "AEX_USERROLE", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "AexUserrole.findAll", query = "SELECT a FROM AexUserrole a")})
public class AexUserrole implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "USROCONS", nullable = false, precision = 22, scale = 10)
    private BigDecimal usrocons;
    @JoinColumn(name = "USROMORO", referencedColumnName = "MOROCONS", nullable = false)
    @ManyToOne(optional = false)
    private AexModurope usromoro;
    @JoinColumn(name = "USROPERF", referencedColumnName = "PERFCONS", nullable = false)
    @ManyToOne(optional = false)
    private AexPerfil usroperf;

    public AexUserrole() {
    }

    public AexUserrole(BigDecimal usrocons) {
        this.usrocons = usrocons;
    }

    public BigDecimal getUsrocons() {
        return usrocons;
    }

    public void setUsrocons(BigDecimal usrocons) {
        this.usrocons = usrocons;
    }

    public AexModurope getUsromoro() {
        return usromoro;
    }

    public void setUsromoro(AexModurope usromoro) {
        this.usromoro = usromoro;
    }

    public AexPerfil getUsroperf() {
        return usroperf;
    }

    public void setUsroperf(AexPerfil usroperf) {
        this.usroperf = usroperf;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usrocons != null ? usrocons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AexUserrole)) {
            return false;
        }
        AexUserrole other = (AexUserrole) object;
        if ((this.usrocons == null && other.usrocons != null) || (this.usrocons != null && !this.usrocons.equals(other.usrocons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.AexUserrole[ usrocons=" + usrocons + " ]";
    }
    
}
