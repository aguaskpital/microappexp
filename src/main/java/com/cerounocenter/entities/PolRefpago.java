/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "POL_REFPAGO", catalog = "", schema = "APPEXP", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"REPAREPA", "REPATRAN"})})
@NamedQueries({
    @NamedQuery(name = "PolRefpago.findAll", query = "SELECT p FROM PolRefpago p")})
public class PolRefpago implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "REPACONS", nullable = false, precision = 22, scale = 10)
    private BigDecimal repacons;
    @Column(name = "REPAREPA", length = 50)
    private String reparepa;
    @Column(name = "REPAVATO", precision = 22, scale = 2)
    private BigDecimal repavato;
    @Column(name = "REPATIPO", precision = 22, scale = 10)
    private BigDecimal repatipo;
    @Column(name = "REPACLIE", precision = 22, scale = 10)
    private BigDecimal repaclie;
    @Column(name = "REPAPAOP", length = 5)
    private String repapaop;
    @JoinColumn(name = "REPATRAN", referencedColumnName = "TRANCONS")
    @ManyToOne
    private PolTransaccion repatran;

    public PolRefpago() {
    }

    public PolRefpago(BigDecimal repacons) {
        this.repacons = repacons;
    }

    public BigDecimal getRepacons() {
        return repacons;
    }

    public void setRepacons(BigDecimal repacons) {
        this.repacons = repacons;
    }

    public String getReparepa() {
        return reparepa;
    }

    public void setReparepa(String reparepa) {
        this.reparepa = reparepa;
    }

    public BigDecimal getRepavato() {
        return repavato;
    }

    public void setRepavato(BigDecimal repavato) {
        this.repavato = repavato;
    }

    public BigDecimal getRepatipo() {
        return repatipo;
    }

    public void setRepatipo(BigDecimal repatipo) {
        this.repatipo = repatipo;
    }

    public BigDecimal getRepaclie() {
        return repaclie;
    }

    public void setRepaclie(BigDecimal repaclie) {
        this.repaclie = repaclie;
    }

    public String getRepapaop() {
        return repapaop;
    }

    public void setRepapaop(String repapaop) {
        this.repapaop = repapaop;
    }

    public PolTransaccion getRepatran() {
        return repatran;
    }

    public void setRepatran(PolTransaccion repatran) {
        this.repatran = repatran;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (repacons != null ? repacons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PolRefpago)) {
            return false;
        }
        PolRefpago other = (PolRefpago) object;
        if ((this.repacons == null && other.repacons != null) || (this.repacons != null && !this.repacons.equals(other.repacons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.PolRefpago[ repacons=" + repacons + " ]";
    }
    
}
