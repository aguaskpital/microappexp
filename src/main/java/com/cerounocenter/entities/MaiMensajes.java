/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "MAI_MENSAJES", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "MaiMensajes.findAll", query = "SELECT m FROM MaiMensajes m")})
public class MaiMensajes implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "MENSCONS", nullable = false, precision = 0, scale = -127)
    private BigDecimal menscons;
    @Basic(optional = false)
    @Column(name = "MENSASUN", nullable = false, length = 2000)
    private String mensasun;
    @Basic(optional = false)
    @Lob
    @Column(name = "MENSCONT", nullable = false)
    private String menscont;
    @Basic(optional = false)
    @Column(name = "MENSFECR", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date mensfecr;
    @Basic(optional = false)
    @Column(name = "MENSELIM", nullable = false)
    private short menselim;
    @Basic(optional = false)
    @Column(name = "MENSBORR", nullable = false)
    private short mensborr;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "envimens")
    private List<MaiEnvios> maiEnviosList;
    @JoinColumn(name = "MENSUSCR", referencedColumnName = "USUACONS", nullable = false)
    @ManyToOne(optional = false)
    private AexUsuario mensuscr;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "adjumens")
    private List<MaiAdjuntos> maiAdjuntosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "entrmens")
    private List<MaiEntregas> maiEntregasList;

    public MaiMensajes() {
    }

    public MaiMensajes(BigDecimal menscons) {
        this.menscons = menscons;
    }

    public MaiMensajes(BigDecimal menscons, String mensasun, String menscont, Date mensfecr, short menselim, short mensborr) {
        this.menscons = menscons;
        this.mensasun = mensasun;
        this.menscont = menscont;
        this.mensfecr = mensfecr;
        this.menselim = menselim;
        this.mensborr = mensborr;
    }

    public BigDecimal getMenscons() {
        return menscons;
    }

    public void setMenscons(BigDecimal menscons) {
        this.menscons = menscons;
    }

    public String getMensasun() {
        return mensasun;
    }

    public void setMensasun(String mensasun) {
        this.mensasun = mensasun;
    }

    public String getMenscont() {
        return menscont;
    }

    public void setMenscont(String menscont) {
        this.menscont = menscont;
    }

    public Date getMensfecr() {
        return mensfecr;
    }

    public void setMensfecr(Date mensfecr) {
        this.mensfecr = mensfecr;
    }

    public short getMenselim() {
        return menselim;
    }

    public void setMenselim(short menselim) {
        this.menselim = menselim;
    }

    public short getMensborr() {
        return mensborr;
    }

    public void setMensborr(short mensborr) {
        this.mensborr = mensborr;
    }

    public List<MaiEnvios> getMaiEnviosList() {
        return maiEnviosList;
    }

    public void setMaiEnviosList(List<MaiEnvios> maiEnviosList) {
        this.maiEnviosList = maiEnviosList;
    }

    public AexUsuario getMensuscr() {
        return mensuscr;
    }

    public void setMensuscr(AexUsuario mensuscr) {
        this.mensuscr = mensuscr;
    }

    public List<MaiAdjuntos> getMaiAdjuntosList() {
        return maiAdjuntosList;
    }

    public void setMaiAdjuntosList(List<MaiAdjuntos> maiAdjuntosList) {
        this.maiAdjuntosList = maiAdjuntosList;
    }

    public List<MaiEntregas> getMaiEntregasList() {
        return maiEntregasList;
    }

    public void setMaiEntregasList(List<MaiEntregas> maiEntregasList) {
        this.maiEntregasList = maiEntregasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (menscons != null ? menscons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MaiMensajes)) {
            return false;
        }
        MaiMensajes other = (MaiMensajes) object;
        if ((this.menscons == null && other.menscons != null) || (this.menscons != null && !this.menscons.equals(other.menscons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.MaiMensajes[ menscons=" + menscons + " ]";
    }
    
}
