/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "AEM_REPORTE", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "AemReporte.findAll", query = "SELECT a FROM AemReporte a")})
public class AemReporte implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "REPOCONS", nullable = false, precision = 20, scale = 0)
    private BigDecimal repocons;
    @Basic(optional = false)
    @Column(name = "REPOTIPO", nullable = false, length = 50)
    private String repotipo;
    @Column(name = "REPOCOOR", length = 50)
    private String repocoor;
    @Column(name = "REPODIRE", length = 200)
    private String repodire;
    @Column(name = "REPOSOLI")
    private BigInteger reposoli;
    @Column(name = "REPOFECR")
    @Temporal(TemporalType.TIMESTAMP)
    private Date repofecr;
    @Column(name = "REPOESTA", length = 20)
    private String repoesta;
    @Column(name = "REPOPURE", length = 4000)
    private String repopure;
    @Column(name = "REPOCONT")
    private BigInteger repocont;
    @Column(name = "REPODEPE", length = 200)
    private String repodepe;
    @JoinColumn(name = "REPOUSUA", referencedColumnName = "USUACONS", nullable = false)
    @ManyToOne(optional = false)
    private AexUsuario repousua;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reimrepo")
    private List<AemRepoimag> aemRepoimagList;

    public AemReporte() {
    }

    public AemReporte(BigDecimal repocons) {
        this.repocons = repocons;
    }

    public AemReporte(BigDecimal repocons, String repotipo) {
        this.repocons = repocons;
        this.repotipo = repotipo;
    }

    public BigDecimal getRepocons() {
        return repocons;
    }

    public void setRepocons(BigDecimal repocons) {
        this.repocons = repocons;
    }

    public String getRepotipo() {
        return repotipo;
    }

    public void setRepotipo(String repotipo) {
        this.repotipo = repotipo;
    }

    public String getRepocoor() {
        return repocoor;
    }

    public void setRepocoor(String repocoor) {
        this.repocoor = repocoor;
    }

    public String getRepodire() {
        return repodire;
    }

    public void setRepodire(String repodire) {
        this.repodire = repodire;
    }

    public BigInteger getReposoli() {
        return reposoli;
    }

    public void setReposoli(BigInteger reposoli) {
        this.reposoli = reposoli;
    }

    public Date getRepofecr() {
        return repofecr;
    }

    public void setRepofecr(Date repofecr) {
        this.repofecr = repofecr;
    }

    public String getRepoesta() {
        return repoesta;
    }

    public void setRepoesta(String repoesta) {
        this.repoesta = repoesta;
    }

    public String getRepopure() {
        return repopure;
    }

    public void setRepopure(String repopure) {
        this.repopure = repopure;
    }

    public BigInteger getRepocont() {
        return repocont;
    }

    public void setRepocont(BigInteger repocont) {
        this.repocont = repocont;
    }

    public String getRepodepe() {
        return repodepe;
    }

    public void setRepodepe(String repodepe) {
        this.repodepe = repodepe;
    }

    public AexUsuario getRepousua() {
        return repousua;
    }

    public void setRepousua(AexUsuario repousua) {
        this.repousua = repousua;
    }

    public List<AemRepoimag> getAemRepoimagList() {
        return aemRepoimagList;
    }

    public void setAemRepoimagList(List<AemRepoimag> aemRepoimagList) {
        this.aemRepoimagList = aemRepoimagList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (repocons != null ? repocons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AemReporte)) {
            return false;
        }
        AemReporte other = (AemReporte) object;
        if ((this.repocons == null && other.repocons != null) || (this.repocons != null && !this.repocons.equals(other.repocons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.AemReporte[ repocons=" + repocons + " ]";
    }
    
}
