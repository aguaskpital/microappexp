/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "MAI_ARCHIVOS", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "MaiArchivos.findAll", query = "SELECT m FROM MaiArchivos m")})
public class MaiArchivos implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ARCHCONS", nullable = false, precision = 0, scale = -127)
    private BigDecimal archcons;
    @Basic(optional = false)
    @Lob
    @Column(name = "ARCHCONT", nullable = false)
    private String archcont;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "adjuarch")
    private List<MaiAdjuntos> maiAdjuntosList;

    public MaiArchivos() {
    }

    public MaiArchivos(BigDecimal archcons) {
        this.archcons = archcons;
    }

    public MaiArchivos(BigDecimal archcons, String archcont) {
        this.archcons = archcons;
        this.archcont = archcont;
    }

    public BigDecimal getArchcons() {
        return archcons;
    }

    public void setArchcons(BigDecimal archcons) {
        this.archcons = archcons;
    }

    public String getArchcont() {
        return archcont;
    }

    public void setArchcont(String archcont) {
        this.archcont = archcont;
    }

    public List<MaiAdjuntos> getMaiAdjuntosList() {
        return maiAdjuntosList;
    }

    public void setMaiAdjuntosList(List<MaiAdjuntos> maiAdjuntosList) {
        this.maiAdjuntosList = maiAdjuntosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (archcons != null ? archcons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MaiArchivos)) {
            return false;
        }
        MaiArchivos other = (MaiArchivos) object;
        if ((this.archcons == null && other.archcons != null) || (this.archcons != null && !this.archcons.equals(other.archcons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.MaiArchivos[ archcons=" + archcons + " ]";
    }
    
}
