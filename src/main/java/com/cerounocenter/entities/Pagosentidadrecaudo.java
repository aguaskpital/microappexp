/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author CEROUNOCENTER S.A.S
 */
@Entity
@Table(name = "PAGOSENTIDADRECAUDO", catalog = "", schema = "APPEXP")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pagosentidadrecaudo.findAll", query = "SELECT p FROM Pagosentidadrecaudo p")
    , @NamedQuery(name = "Pagosentidadrecaudo.findByPaencons", query = "SELECT p FROM Pagosentidadrecaudo p WHERE p.paencons = :paencons")
    , @NamedQuery(name = "Pagosentidadrecaudo.findByPaencont", query = "SELECT p FROM Pagosentidadrecaudo p WHERE p.paencont = :paencont")
    , @NamedQuery(name = "Pagosentidadrecaudo.findByPaenenre", query = "SELECT p FROM Pagosentidadrecaudo p WHERE p.paenenre = :paenenre")
    , @NamedQuery(name = "Pagosentidadrecaudo.findByPaenvapa", query = "SELECT p FROM Pagosentidadrecaudo p WHERE p.paenvapa = :paenvapa")
    , @NamedQuery(name = "Pagosentidadrecaudo.findByPaenfepa", query = "SELECT p FROM Pagosentidadrecaudo p WHERE p.paenfepa = :paenfepa")
    , @NamedQuery(name = "Pagosentidadrecaudo.findByPaencoba", query = "SELECT p FROM Pagosentidadrecaudo p WHERE p.paencoba = :paencoba")
    , @NamedQuery(name = "Pagosentidadrecaudo.findByPaentipa", query = "SELECT p FROM Pagosentidadrecaudo p WHERE p.paentipa = :paentipa")
    , @NamedQuery(name = "Pagosentidadrecaudo.findByPaencupo", query = "SELECT p FROM Pagosentidadrecaudo p WHERE p.paencupo = :paencupo")
    , @NamedQuery(name = "Pagosentidadrecaudo.findByPaenesta", query = "SELECT p FROM Pagosentidadrecaudo p WHERE p.paenesta = :paenesta")
    , @NamedQuery(name = "Pagosentidadrecaudo.findByPaenusua", query = "SELECT p FROM Pagosentidadrecaudo p WHERE p.paenusua = :paenusua")
    , @NamedQuery(name = "Pagosentidadrecaudo.findByPaenipus", query = "SELECT p FROM Pagosentidadrecaudo p WHERE p.paenipus = :paenipus")})
public class Pagosentidadrecaudo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PAENCONS", nullable = false)
    private Long paencons;
    @Basic(optional = false)
    @Column(name = "PAENCONT", nullable = false)
    private long paencont;
    @Basic(optional = false)
    @Column(name = "PAENENRE", nullable = false, length = 30)
    private String paenenre;
    @Basic(optional = false)
    @Column(name = "PAENVAPA", nullable = false)
    private long paenvapa;
    @Basic(optional = false)
    @Column(name = "PAENFEPA", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date paenfepa;
    @Basic(optional = false)
    @Column(name = "PAENCOBA", nullable = false, length = 80)
    private String paencoba;
    @Basic(optional = false)
    @Column(name = "PAENTIPA", nullable = false)
    private long paentipa;
    @Basic(optional = false)
    @Column(name = "PAENCUPO", nullable = false)
    private long paencupo;
    @Basic(optional = false)
    @Column(name = "PAENESTA", nullable = false, length = 20)
    private String paenesta;
    @Basic(optional = false)
    @Column(name = "PAENUSUA", nullable = false)
    private long paenusua;
    @Column(name = "PAENIPUS", length = 20)
    private String paenipus;

    public Pagosentidadrecaudo() {
    }

    public Pagosentidadrecaudo(Long paencons) {
        this.paencons = paencons;
    }

    public Pagosentidadrecaudo(Long paencons, long paencont, String paenenre, long paenvapa, Date paenfepa, String paencoba, long paentipa, long paencupo, String paenesta, long paenusua) {
        this.paencons = paencons;
        this.paencont = paencont;
        this.paenenre = paenenre;
        this.paenvapa = paenvapa;
        this.paenfepa = paenfepa;
        this.paencoba = paencoba;
        this.paentipa = paentipa;
        this.paencupo = paencupo;
        this.paenesta = paenesta;
        this.paenusua = paenusua;
    }

    public Long getPaencons() {
        return paencons;
    }

    public void setPaencons(Long paencons) {
        this.paencons = paencons;
    }

    public long getPaencont() {
        return paencont;
    }

    public void setPaencont(long paencont) {
        this.paencont = paencont;
    }

    public String getPaenenre() {
        return paenenre;
    }

    public void setPaenenre(String paenenre) {
        this.paenenre = paenenre;
    }

    public long getPaenvapa() {
        return paenvapa;
    }

    public void setPaenvapa(long paenvapa) {
        this.paenvapa = paenvapa;
    }

    public Date getPaenfepa() {
        return paenfepa;
    }

    public void setPaenfepa(Date paenfepa) {
        this.paenfepa = paenfepa;
    }

    public String getPaencoba() {
        return paencoba;
    }

    public void setPaencoba(String paencoba) {
        this.paencoba = paencoba;
    }

    public long getPaentipa() {
        return paentipa;
    }

    public void setPaentipa(long paentipa) {
        this.paentipa = paentipa;
    }

    public long getPaencupo() {
        return paencupo;
    }

    public void setPaencupo(long paencupo) {
        this.paencupo = paencupo;
    }

    public String getPaenesta() {
        return paenesta;
    }

    public void setPaenesta(String paenesta) {
        this.paenesta = paenesta;
    }

    public long getPaenusua() {
        return paenusua;
    }

    public void setPaenusua(long paenusua) {
        this.paenusua = paenusua;
    }

    public String getPaenipus() {
        return paenipus;
    }

    public void setPaenipus(String paenipus) {
        this.paenipus = paenipus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (paencons != null ? paencons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pagosentidadrecaudo)) {
            return false;
        }
        Pagosentidadrecaudo other = (Pagosentidadrecaudo) object;
        if ((this.paencons == null && other.paencons != null) || (this.paencons != null && !this.paencons.equals(other.paencons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.Pagosentidadrecaudo[ paencons=" + paencons + " ]";
    }
    
}
