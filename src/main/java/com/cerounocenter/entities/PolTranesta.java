/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "POL_TRANESTA", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "PolTranesta.findAll", query = "SELECT p FROM PolTranesta p")})
public class PolTranesta implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "TRESCONS", nullable = false, precision = 22, scale = 10)
    private BigDecimal trescons;
    @Column(name = "TRESESAN", precision = 22, scale = 10)
    private BigDecimal tresesan;
    @Column(name = "TRESESAC", precision = 22, scale = 10)
    private BigDecimal tresesac;
    @Column(name = "TRESFECH")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tresfech;
    @Column(name = "TRESUSER", precision = 22, scale = 10)
    private BigDecimal tresuser;
    @JoinColumn(name = "TRESTRAN", referencedColumnName = "TRANCONS")
    @ManyToOne
    private PolTransaccion trestran;

    public PolTranesta() {
    }

    public PolTranesta(BigDecimal trescons) {
        this.trescons = trescons;
    }

    public BigDecimal getTrescons() {
        return trescons;
    }

    public void setTrescons(BigDecimal trescons) {
        this.trescons = trescons;
    }

    public BigDecimal getTresesan() {
        return tresesan;
    }

    public void setTresesan(BigDecimal tresesan) {
        this.tresesan = tresesan;
    }

    public BigDecimal getTresesac() {
        return tresesac;
    }

    public void setTresesac(BigDecimal tresesac) {
        this.tresesac = tresesac;
    }

    public Date getTresfech() {
        return tresfech;
    }

    public void setTresfech(Date tresfech) {
        this.tresfech = tresfech;
    }

    public BigDecimal getTresuser() {
        return tresuser;
    }

    public void setTresuser(BigDecimal tresuser) {
        this.tresuser = tresuser;
    }

    public PolTransaccion getTrestran() {
        return trestran;
    }

    public void setTrestran(PolTransaccion trestran) {
        this.trestran = trestran;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (trescons != null ? trescons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PolTranesta)) {
            return false;
        }
        PolTranesta other = (PolTranesta) object;
        if ((this.trescons == null && other.trescons != null) || (this.trescons != null && !this.trescons.equals(other.trescons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.PolTranesta[ trescons=" + trescons + " ]";
    }
    
}
