/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "CAL_PKID", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "CalPkid.findAll", query = "SELECT c FROM CalPkid c")})
public class CalPkid implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PKID", nullable = false, length = 20)
    private String pkid;
    @Column(name = "PKIDVALU")
    private Long pkidvalu;

    public CalPkid() {
    }

    public CalPkid(String pkid) {
        this.pkid = pkid;
    }

    public String getPkid() {
        return pkid;
    }

    public void setPkid(String pkid) {
        this.pkid = pkid;
    }

    public Long getPkidvalu() {
        return pkidvalu;
    }

    public void setPkidvalu(Long pkidvalu) {
        this.pkidvalu = pkidvalu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkid != null ? pkid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CalPkid)) {
            return false;
        }
        CalPkid other = (CalPkid) object;
        if ((this.pkid == null && other.pkid != null) || (this.pkid != null && !this.pkid.equals(other.pkid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.CalPkid[ pkid=" + pkid + " ]";
    }
    
}
