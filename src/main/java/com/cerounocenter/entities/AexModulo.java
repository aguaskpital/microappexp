/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "AEX_MODULO", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "AexModulo.findAll", query = "SELECT a FROM AexModulo a")})
public class AexModulo implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "MODUCONS", nullable = false, precision = 22, scale = 10)
    private BigDecimal moducons;
    @Column(name = "MODUNOMB", length = 1000)
    private String modunomb;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "moromodu")
    private List<AexModurope> aexModuropeList;
    @JoinColumn(name = "MODUAPPL", referencedColumnName = "APPLCONS", nullable = false)
    @ManyToOne(optional = false)
    private AexApp moduappl;

    public AexModulo() {
    }

    public AexModulo(BigDecimal moducons) {
        this.moducons = moducons;
    }

    public BigDecimal getModucons() {
        return moducons;
    }

    public void setModucons(BigDecimal moducons) {
        this.moducons = moducons;
    }

    public String getModunomb() {
        return modunomb;
    }

    public void setModunomb(String modunomb) {
        this.modunomb = modunomb;
    }

    public List<AexModurope> getAexModuropeList() {
        return aexModuropeList;
    }

    public void setAexModuropeList(List<AexModurope> aexModuropeList) {
        this.aexModuropeList = aexModuropeList;
    }

    public AexApp getModuappl() {
        return moduappl;
    }

    public void setModuappl(AexApp moduappl) {
        this.moduappl = moduappl;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (moducons != null ? moducons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AexModulo)) {
            return false;
        }
        AexModulo other = (AexModulo) object;
        if ((this.moducons == null && other.moducons != null) || (this.moducons != null && !this.moducons.equals(other.moducons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.AexModulo[ moducons=" + moducons + " ]";
    }
    
}
