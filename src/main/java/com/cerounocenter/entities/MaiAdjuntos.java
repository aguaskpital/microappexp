/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "MAI_ADJUNTOS", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "MaiAdjuntos.findAll", query = "SELECT m FROM MaiAdjuntos m")})
public class MaiAdjuntos implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ADJUCONS", nullable = false, precision = 0, scale = -127)
    private BigDecimal adjucons;
    @Basic(optional = false)
    @Column(name = "ADJUNOMB", nullable = false, length = 255)
    private String adjunomb;
    @Basic(optional = false)
    @Column(name = "ADJUTIPO", nullable = false, length = 255)
    private String adjutipo;
    @Basic(optional = false)
    @Column(name = "ADJUTAMA", nullable = false)
    private BigInteger adjutama;
    @JoinColumn(name = "ADJUARCH", referencedColumnName = "ARCHCONS", nullable = false)
    @ManyToOne(optional = false)
    private MaiArchivos adjuarch;
    @JoinColumn(name = "ADJUMENS", referencedColumnName = "MENSCONS", nullable = false)
    @ManyToOne(optional = false)
    private MaiMensajes adjumens;

    public MaiAdjuntos() {
    }

    public MaiAdjuntos(BigDecimal adjucons) {
        this.adjucons = adjucons;
    }

    public MaiAdjuntos(BigDecimal adjucons, String adjunomb, String adjutipo, BigInteger adjutama) {
        this.adjucons = adjucons;
        this.adjunomb = adjunomb;
        this.adjutipo = adjutipo;
        this.adjutama = adjutama;
    }

    public BigDecimal getAdjucons() {
        return adjucons;
    }

    public void setAdjucons(BigDecimal adjucons) {
        this.adjucons = adjucons;
    }

    public String getAdjunomb() {
        return adjunomb;
    }

    public void setAdjunomb(String adjunomb) {
        this.adjunomb = adjunomb;
    }

    public String getAdjutipo() {
        return adjutipo;
    }

    public void setAdjutipo(String adjutipo) {
        this.adjutipo = adjutipo;
    }

    public BigInteger getAdjutama() {
        return adjutama;
    }

    public void setAdjutama(BigInteger adjutama) {
        this.adjutama = adjutama;
    }

    public MaiArchivos getAdjuarch() {
        return adjuarch;
    }

    public void setAdjuarch(MaiArchivos adjuarch) {
        this.adjuarch = adjuarch;
    }

    public MaiMensajes getAdjumens() {
        return adjumens;
    }

    public void setAdjumens(MaiMensajes adjumens) {
        this.adjumens = adjumens;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (adjucons != null ? adjucons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MaiAdjuntos)) {
            return false;
        }
        MaiAdjuntos other = (MaiAdjuntos) object;
        if ((this.adjucons == null && other.adjucons != null) || (this.adjucons != null && !this.adjucons.equals(other.adjucons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.MaiAdjuntos[ adjucons=" + adjucons + " ]";
    }
    
}
