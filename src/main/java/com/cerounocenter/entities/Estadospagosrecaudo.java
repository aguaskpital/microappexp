/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "ESTADOSPAGOSRECAUDO", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "Estadospagosrecaudo.findAll", query = "SELECT e FROM Estadospagosrecaudo e")})
public class Estadospagosrecaudo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ESPACONS", nullable = false)
    private Long espacons;
    @Basic(optional = false)
    @Column(name = "ESPAESAN", nullable = false, length = 20)
    private String espaesan;
    @Basic(optional = false)
    @Column(name = "ESPAESNU", nullable = false, length = 20)
    private String espaesnu;
    @Basic(optional = false)
    @Column(name = "ESPAFERE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date espafere;
    @Basic(optional = false)
    @Column(name = "ESPAUSUA", nullable = false)
    private long espausua;
    @Column(name = "ESPAIPUS", length = 20)
    private String espaipus;
    @JoinColumn(name = "ESPAPAEN", referencedColumnName = "PAENCONS", nullable = false)
    @ManyToOne(optional = false)
    private Pagosentidadrecaudo espapaen;

    public Estadospagosrecaudo() {
    }

    public Estadospagosrecaudo(Long espacons) {
        this.espacons = espacons;
    }

    public Estadospagosrecaudo(Long espacons, String espaesan, String espaesnu, Date espafere, long espausua) {
        this.espacons = espacons;
        this.espaesan = espaesan;
        this.espaesnu = espaesnu;
        this.espafere = espafere;
        this.espausua = espausua;
    }

    public Long getEspacons() {
        return espacons;
    }

    public void setEspacons(Long espacons) {
        this.espacons = espacons;
    }

    public String getEspaesan() {
        return espaesan;
    }

    public void setEspaesan(String espaesan) {
        this.espaesan = espaesan;
    }

    public String getEspaesnu() {
        return espaesnu;
    }

    public void setEspaesnu(String espaesnu) {
        this.espaesnu = espaesnu;
    }

    public Date getEspafere() {
        return espafere;
    }

    public void setEspafere(Date espafere) {
        this.espafere = espafere;
    }

    public long getEspausua() {
        return espausua;
    }

    public void setEspausua(long espausua) {
        this.espausua = espausua;
    }

    public String getEspaipus() {
        return espaipus;
    }

    public void setEspaipus(String espaipus) {
        this.espaipus = espaipus;
    }

    public Pagosentidadrecaudo getEspapaen() {
        return espapaen;
    }

    public void setEspapaen(Pagosentidadrecaudo espapaen) {
        this.espapaen = espapaen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (espacons != null ? espacons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estadospagosrecaudo)) {
            return false;
        }
        Estadospagosrecaudo other = (Estadospagosrecaudo) object;
        if ((this.espacons == null && other.espacons != null) || (this.espacons != null && !this.espacons.equals(other.espacons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.Estadospagosrecaudo[ espacons=" + espacons + " ]";
    }
    
}
