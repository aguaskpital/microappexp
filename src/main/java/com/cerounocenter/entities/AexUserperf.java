/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "AEX_USERPERF", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "AexUserperf.findAll", query = "SELECT a FROM AexUserperf a")})
public class AexUserperf implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "USPECONS", nullable = false, precision = 22, scale = 10)
    private BigDecimal uspecons;
    @Basic(optional = false)
    @Column(name = "USPEESTA", nullable = false, precision = 22, scale = 10)
    private BigDecimal uspeesta;
    @JoinColumn(name = "USPEPERF", referencedColumnName = "PERFCONS", nullable = false)
    @ManyToOne(optional = false)
    private AexPerfil uspeperf;
    @JoinColumn(name = "USPEUSUA", referencedColumnName = "USUACONS", nullable = false)
    @ManyToOne(optional = false)
    private AexUsuario uspeusua;

    public AexUserperf() {
    }

    public AexUserperf(BigDecimal uspecons) {
        this.uspecons = uspecons;
    }

    public AexUserperf(BigDecimal uspecons, BigDecimal uspeesta) {
        this.uspecons = uspecons;
        this.uspeesta = uspeesta;
    }

    public BigDecimal getUspecons() {
        return uspecons;
    }

    public void setUspecons(BigDecimal uspecons) {
        this.uspecons = uspecons;
    }

    public BigDecimal getUspeesta() {
        return uspeesta;
    }

    public void setUspeesta(BigDecimal uspeesta) {
        this.uspeesta = uspeesta;
    }

    public AexPerfil getUspeperf() {
        return uspeperf;
    }

    public void setUspeperf(AexPerfil uspeperf) {
        this.uspeperf = uspeperf;
    }

    public AexUsuario getUspeusua() {
        return uspeusua;
    }

    public void setUspeusua(AexUsuario uspeusua) {
        this.uspeusua = uspeusua;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (uspecons != null ? uspecons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AexUserperf)) {
            return false;
        }
        AexUserperf other = (AexUserperf) object;
        if ((this.uspecons == null && other.uspecons != null) || (this.uspecons != null && !this.uspecons.equals(other.uspecons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.AexUserperf[ uspecons=" + uspecons + " ]";
    }
    
}
