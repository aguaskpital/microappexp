/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "AEM_REPOIMAG", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "AemRepoimag.findAll", query = "SELECT a FROM AemRepoimag a")})
public class AemRepoimag implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "REIMCONS", nullable = false, precision = 20, scale = 0)
    private BigDecimal reimcons;
    @Lob
    @Column(name = "REIMIMAG")
    private String reimimag;
    @Column(name = "REIMFECR")
    @Temporal(TemporalType.TIMESTAMP)
    private Date reimfecr;
    @Column(name = "REIMESTA", length = 20)
    private String reimesta;
    @JoinColumn(name = "REIMREPO", referencedColumnName = "REPOCONS", nullable = false)
    @ManyToOne(optional = false)
    private AemReporte reimrepo;

    public AemRepoimag() {
    }

    public AemRepoimag(BigDecimal reimcons) {
        this.reimcons = reimcons;
    }

    public BigDecimal getReimcons() {
        return reimcons;
    }

    public void setReimcons(BigDecimal reimcons) {
        this.reimcons = reimcons;
    }

    public String getReimimag() {
        return reimimag;
    }

    public void setReimimag(String reimimag) {
        this.reimimag = reimimag;
    }

    public Date getReimfecr() {
        return reimfecr;
    }

    public void setReimfecr(Date reimfecr) {
        this.reimfecr = reimfecr;
    }

    public String getReimesta() {
        return reimesta;
    }

    public void setReimesta(String reimesta) {
        this.reimesta = reimesta;
    }

    public AemReporte getReimrepo() {
        return reimrepo;
    }

    public void setReimrepo(AemReporte reimrepo) {
        this.reimrepo = reimrepo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reimcons != null ? reimcons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AemRepoimag)) {
            return false;
        }
        AemRepoimag other = (AemRepoimag) object;
        if ((this.reimcons == null && other.reimcons != null) || (this.reimcons != null && !this.reimcons.equals(other.reimcons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.AemRepoimag[ reimcons=" + reimcons + " ]";
    }
    
}
