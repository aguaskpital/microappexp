/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "POL_DETALLE", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "PolDetalle.findAll", query = "SELECT p FROM PolDetalle p")})
public class PolDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "DETACONS", nullable = false, precision = 22, scale = 10)
    private BigDecimal detacons;
    @Column(name = "DETACAMP", length = 50)
    private String detacamp;
    @Column(name = "DETAVALO", length = 50)
    private String detavalo;
    @JoinColumn(name = "DETATRAN", referencedColumnName = "TRANCONS")
    @ManyToOne
    private PolTransaccion detatran;

    public PolDetalle() {
    }

    public PolDetalle(BigDecimal detacons) {
        this.detacons = detacons;
    }

    public BigDecimal getDetacons() {
        return detacons;
    }

    public void setDetacons(BigDecimal detacons) {
        this.detacons = detacons;
    }

    public String getDetacamp() {
        return detacamp;
    }

    public void setDetacamp(String detacamp) {
        this.detacamp = detacamp;
    }

    public String getDetavalo() {
        return detavalo;
    }

    public void setDetavalo(String detavalo) {
        this.detavalo = detavalo;
    }

    public PolTransaccion getDetatran() {
        return detatran;
    }

    public void setDetatran(PolTransaccion detatran) {
        this.detatran = detatran;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (detacons != null ? detacons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PolDetalle)) {
            return false;
        }
        PolDetalle other = (PolDetalle) object;
        if ((this.detacons == null && other.detacons != null) || (this.detacons != null && !this.detacons.equals(other.detacons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.PolDetalle[ detacons=" + detacons + " ]";
    }
    
}
