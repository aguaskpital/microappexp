/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "CAL_IMAGCODI", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "CalImagcodi.findAll", query = "SELECT c FROM CalImagcodi c")})
public class CalImagcodi implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "IMCOCONS", nullable = false)
    private Long imcocons;
    @Column(name = "IMCOTIPO")
    private Short imcotipo;
    @Column(name = "IMCOEXTE", length = 20)
    private String imcoexte;
    @Lob
    @Column(name = "IMAGCODI")
    private String imagcodi;

    public CalImagcodi() {
    }

    public CalImagcodi(Long imcocons) {
        this.imcocons = imcocons;
    }

    public Long getImcocons() {
        return imcocons;
    }

    public void setImcocons(Long imcocons) {
        this.imcocons = imcocons;
    }

    public Short getImcotipo() {
        return imcotipo;
    }

    public void setImcotipo(Short imcotipo) {
        this.imcotipo = imcotipo;
    }

    public String getImcoexte() {
        return imcoexte;
    }

    public void setImcoexte(String imcoexte) {
        this.imcoexte = imcoexte;
    }

    public String getImagcodi() {
        return imagcodi;
    }

    public void setImagcodi(String imagcodi) {
        this.imagcodi = imagcodi;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (imcocons != null ? imcocons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CalImagcodi)) {
            return false;
        }
        CalImagcodi other = (CalImagcodi) object;
        if ((this.imcocons == null && other.imcocons != null) || (this.imcocons != null && !this.imcocons.equals(other.imcocons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.CalImagcodi[ imcocons=" + imcocons + " ]";
    }
    
}
