/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "AEX_ARBOL", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "AexArbol.findAll", query = "SELECT a FROM AexArbol a")})
public class AexArbol implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ARBOCONS", nullable = false, precision = 22, scale = 10)
    private BigDecimal arbocons;
    @Basic(optional = false)
    @Column(name = "ARBOPADR", nullable = false, precision = 22, scale = 10)
    private BigDecimal arbopadr;
    @Column(name = "ARBONAME", length = 1000)
    private String arboname;
    @Column(name = "ARBOICON", length = 1000)
    private String arboicon;
    @Basic(optional = false)
    @Column(name = "ARBOPOS", nullable = false, precision = 22, scale = 10)
    private BigDecimal arbopos;
    @Basic(optional = false)
    @Column(name = "ARBOESTA", nullable = false, precision = 22, scale = 10)
    private BigDecimal arboesta;
    @Column(name = "ARBORUTA", length = 1000)
    private String arboruta;
    @Column(name = "ARBOCSS", length = 1000)
    private String arbocss;
    @Basic(optional = false)
    @Column(name = "ARBOMODU", nullable = false, precision = 22, scale = 10)
    private BigDecimal arbomodu;

    public AexArbol() {
    }

    public AexArbol(BigDecimal arbocons) {
        this.arbocons = arbocons;
    }

    public AexArbol(BigDecimal arbocons, BigDecimal arbopadr, BigDecimal arbopos, BigDecimal arboesta, BigDecimal arbomodu) {
        this.arbocons = arbocons;
        this.arbopadr = arbopadr;
        this.arbopos = arbopos;
        this.arboesta = arboesta;
        this.arbomodu = arbomodu;
    }

    public BigDecimal getArbocons() {
        return arbocons;
    }

    public void setArbocons(BigDecimal arbocons) {
        this.arbocons = arbocons;
    }

    public BigDecimal getArbopadr() {
        return arbopadr;
    }

    public void setArbopadr(BigDecimal arbopadr) {
        this.arbopadr = arbopadr;
    }

    public String getArboname() {
        return arboname;
    }

    public void setArboname(String arboname) {
        this.arboname = arboname;
    }

    public String getArboicon() {
        return arboicon;
    }

    public void setArboicon(String arboicon) {
        this.arboicon = arboicon;
    }

    public BigDecimal getArbopos() {
        return arbopos;
    }

    public void setArbopos(BigDecimal arbopos) {
        this.arbopos = arbopos;
    }

    public BigDecimal getArboesta() {
        return arboesta;
    }

    public void setArboesta(BigDecimal arboesta) {
        this.arboesta = arboesta;
    }

    public String getArboruta() {
        return arboruta;
    }

    public void setArboruta(String arboruta) {
        this.arboruta = arboruta;
    }

    public String getArbocss() {
        return arbocss;
    }

    public void setArbocss(String arbocss) {
        this.arbocss = arbocss;
    }

    public BigDecimal getArbomodu() {
        return arbomodu;
    }

    public void setArbomodu(BigDecimal arbomodu) {
        this.arbomodu = arbomodu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (arbocons != null ? arbocons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AexArbol)) {
            return false;
        }
        AexArbol other = (AexArbol) object;
        if ((this.arbocons == null && other.arbocons != null) || (this.arbocons != null && !this.arbocons.equals(other.arbocons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.AexArbol[ arbocons=" + arbocons + " ]";
    }
    
}
