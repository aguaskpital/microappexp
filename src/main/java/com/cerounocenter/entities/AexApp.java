/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "AEX_APP", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "AexApp.findAll", query = "SELECT a FROM AexApp a")})
public class AexApp implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "APPLCONS", nullable = false, precision = 22, scale = 10)
    private BigDecimal applcons;
    @Column(name = "APPLNOMB", length = 1000)
    private String applnomb;
    @Column(name = "APPLFERE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date applfere;
    @Column(name = "APPLDESC", length = 1000)
    private String appldesc;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "moduappl")
    private List<AexModulo> aexModuloList;

    public AexApp() {
    }

    public AexApp(BigDecimal applcons) {
        this.applcons = applcons;
    }

    public BigDecimal getApplcons() {
        return applcons;
    }

    public void setApplcons(BigDecimal applcons) {
        this.applcons = applcons;
    }

    public String getApplnomb() {
        return applnomb;
    }

    public void setApplnomb(String applnomb) {
        this.applnomb = applnomb;
    }

    public Date getApplfere() {
        return applfere;
    }

    public void setApplfere(Date applfere) {
        this.applfere = applfere;
    }

    public String getAppldesc() {
        return appldesc;
    }

    public void setAppldesc(String appldesc) {
        this.appldesc = appldesc;
    }

    public List<AexModulo> getAexModuloList() {
        return aexModuloList;
    }

    public void setAexModuloList(List<AexModulo> aexModuloList) {
        this.aexModuloList = aexModuloList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (applcons != null ? applcons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AexApp)) {
            return false;
        }
        AexApp other = (AexApp) object;
        if ((this.applcons == null && other.applcons != null) || (this.applcons != null && !this.applcons.equals(other.applcons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.AexApp[ applcons=" + applcons + " ]";
    }
    
}
