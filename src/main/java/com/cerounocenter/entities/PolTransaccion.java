/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "POL_TRANSACCION", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "PolTransaccion.findAll", query = "SELECT p FROM PolTransaccion p")})
public class PolTransaccion implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "TRANCONS", nullable = false, precision = 22, scale = 10)
    private BigDecimal trancons;
    @Column(name = "TRANFETR")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tranfetr;
    @Column(name = "TRANXREQ", length = 4000)
    private String tranxreq;
    @Column(name = "TRANXRES", length = 4000)
    private String tranxres;
    @Column(name = "TRANESTA", length = 10)
    private String tranesta;
    @Column(name = "TRANIDUS")
    private BigInteger tranidus;
    @Column(name = "TRANTIPA", precision = 22, scale = 10)
    private BigDecimal trantipa;
    @OneToMany(mappedBy = "repatran")
    private List<PolRefpago> polRefpagoList;
    @OneToMany(mappedBy = "trestran")
    private List<PolTranesta> polTranestaList;
    @JoinColumn(name = "TRANEMPR", referencedColumnName = "EMPRCONS")
    @ManyToOne
    private PolEmpresa tranempr;
    @OneToMany(mappedBy = "detatran")
    private List<PolDetalle> polDetalleList;

    public PolTransaccion() {
    }

    public PolTransaccion(BigDecimal trancons) {
        this.trancons = trancons;
    }

    public BigDecimal getTrancons() {
        return trancons;
    }

    public void setTrancons(BigDecimal trancons) {
        this.trancons = trancons;
    }

    public Date getTranfetr() {
        return tranfetr;
    }

    public void setTranfetr(Date tranfetr) {
        this.tranfetr = tranfetr;
    }

    public String getTranxreq() {
        return tranxreq;
    }

    public void setTranxreq(String tranxreq) {
        this.tranxreq = tranxreq;
    }

    public String getTranxres() {
        return tranxres;
    }

    public void setTranxres(String tranxres) {
        this.tranxres = tranxres;
    }

    public String getTranesta() {
        return tranesta;
    }

    public void setTranesta(String tranesta) {
        this.tranesta = tranesta;
    }

    public BigInteger getTranidus() {
        return tranidus;
    }

    public void setTranidus(BigInteger tranidus) {
        this.tranidus = tranidus;
    }

    public BigDecimal getTrantipa() {
        return trantipa;
    }

    public void setTrantipa(BigDecimal trantipa) {
        this.trantipa = trantipa;
    }

    public List<PolRefpago> getPolRefpagoList() {
        return polRefpagoList;
    }

    public void setPolRefpagoList(List<PolRefpago> polRefpagoList) {
        this.polRefpagoList = polRefpagoList;
    }

    public List<PolTranesta> getPolTranestaList() {
        return polTranestaList;
    }

    public void setPolTranestaList(List<PolTranesta> polTranestaList) {
        this.polTranestaList = polTranestaList;
    }

    public PolEmpresa getTranempr() {
        return tranempr;
    }

    public void setTranempr(PolEmpresa tranempr) {
        this.tranempr = tranempr;
    }

    public List<PolDetalle> getPolDetalleList() {
        return polDetalleList;
    }

    public void setPolDetalleList(List<PolDetalle> polDetalleList) {
        this.polDetalleList = polDetalleList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (trancons != null ? trancons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PolTransaccion)) {
            return false;
        }
        PolTransaccion other = (PolTransaccion) object;
        if ((this.trancons == null && other.trancons != null) || (this.trancons != null && !this.trancons.equals(other.trancons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.PolTransaccion[ trancons=" + trancons + " ]";
    }
    
}
