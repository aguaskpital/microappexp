/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "AEM_TELEFONO", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "AemTelefono.findAll", query = "SELECT a FROM AemTelefono a")})
public class AemTelefono implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "TELECONS", nullable = false)
    private Long telecons;
    @Column(name = "TELEIMEI", length = 50)
    private String teleimei;
    @Column(name = "TELENUME", length = 15)
    private String telenume;
    @Column(name = "TELENOMB", length = 50)
    private String telenomb;
    @Column(name = "TELEPLAT", length = 20)
    private String teleplat;
    @Column(name = "TELESOVE", length = 10)
    private String telesove;
    @Column(name = "TELEFECR")
    @Temporal(TemporalType.TIMESTAMP)
    private Date telefecr;
    @Column(name = "TELEESTA", length = 20)
    private String teleesta;
    @JoinColumn(name = "TELEUSUA", referencedColumnName = "USUACONS", nullable = false)
    @ManyToOne(optional = false)
    private AexUsuario teleusua;

    public AemTelefono() {
    }

    public AemTelefono(Long telecons) {
        this.telecons = telecons;
    }

    public Long getTelecons() {
        return telecons;
    }

    public void setTelecons(Long telecons) {
        this.telecons = telecons;
    }

    public String getTeleimei() {
        return teleimei;
    }

    public void setTeleimei(String teleimei) {
        this.teleimei = teleimei;
    }

    public String getTelenume() {
        return telenume;
    }

    public void setTelenume(String telenume) {
        this.telenume = telenume;
    }

    public String getTelenomb() {
        return telenomb;
    }

    public void setTelenomb(String telenomb) {
        this.telenomb = telenomb;
    }

    public String getTeleplat() {
        return teleplat;
    }

    public void setTeleplat(String teleplat) {
        this.teleplat = teleplat;
    }

    public String getTelesove() {
        return telesove;
    }

    public void setTelesove(String telesove) {
        this.telesove = telesove;
    }

    public Date getTelefecr() {
        return telefecr;
    }

    public void setTelefecr(Date telefecr) {
        this.telefecr = telefecr;
    }

    public String getTeleesta() {
        return teleesta;
    }

    public void setTeleesta(String teleesta) {
        this.teleesta = teleesta;
    }

    public AexUsuario getTeleusua() {
        return teleusua;
    }

    public void setTeleusua(AexUsuario teleusua) {
        this.teleusua = teleusua;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (telecons != null ? telecons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AemTelefono)) {
            return false;
        }
        AemTelefono other = (AemTelefono) object;
        if ((this.telecons == null && other.telecons != null) || (this.telecons != null && !this.telecons.equals(other.telecons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.AemTelefono[ telecons=" + telecons + " ]";
    }
    
}
