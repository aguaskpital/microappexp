/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "MAI_ENTREGAS", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "MaiEntregas.findAll", query = "SELECT m FROM MaiEntregas m")})
public class MaiEntregas implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ENTRCONS", nullable = false, precision = 0, scale = -127)
    private BigDecimal entrcons;
    @Basic(optional = false)
    @Column(name = "ENTRFECH", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date entrfech;
    @Basic(optional = false)
    @Column(name = "ENTRELIM", nullable = false)
    private short entrelim;
    @Basic(optional = false)
    @Column(name = "ENTRLEID", nullable = false)
    private short entrleid;
    @Basic(optional = false)
    @Column(name = "ENTRIMPO", nullable = false)
    private short entrimpo;
    @Basic(optional = false)
    @Column(name = "ENTRABAN", nullable = false)
    private short entraban;
    @JoinColumn(name = "ENTRUSDE", referencedColumnName = "USUACONS", nullable = false)
    @ManyToOne(optional = false)
    private AexUsuario entrusde;
    @JoinColumn(name = "ENTRENVI", referencedColumnName = "ENVICONS", nullable = false)
    @ManyToOne(optional = false)
    private MaiEnvios entrenvi;
    @JoinColumn(name = "ENTRMENS", referencedColumnName = "MENSCONS", nullable = false)
    @ManyToOne(optional = false)
    private MaiMensajes entrmens;
    @JoinColumn(name = "ENTRCATE", referencedColumnName = "TBNUMERO")
    @ManyToOne
    private MaiTablas entrcate;

    public MaiEntregas() {
    }

    public MaiEntregas(BigDecimal entrcons) {
        this.entrcons = entrcons;
    }

    public MaiEntregas(BigDecimal entrcons, Date entrfech, short entrelim, short entrleid, short entrimpo, short entraban) {
        this.entrcons = entrcons;
        this.entrfech = entrfech;
        this.entrelim = entrelim;
        this.entrleid = entrleid;
        this.entrimpo = entrimpo;
        this.entraban = entraban;
    }

    public BigDecimal getEntrcons() {
        return entrcons;
    }

    public void setEntrcons(BigDecimal entrcons) {
        this.entrcons = entrcons;
    }

    public Date getEntrfech() {
        return entrfech;
    }

    public void setEntrfech(Date entrfech) {
        this.entrfech = entrfech;
    }

    public short getEntrelim() {
        return entrelim;
    }

    public void setEntrelim(short entrelim) {
        this.entrelim = entrelim;
    }

    public short getEntrleid() {
        return entrleid;
    }

    public void setEntrleid(short entrleid) {
        this.entrleid = entrleid;
    }

    public short getEntrimpo() {
        return entrimpo;
    }

    public void setEntrimpo(short entrimpo) {
        this.entrimpo = entrimpo;
    }

    public short getEntraban() {
        return entraban;
    }

    public void setEntraban(short entraban) {
        this.entraban = entraban;
    }

    public AexUsuario getEntrusde() {
        return entrusde;
    }

    public void setEntrusde(AexUsuario entrusde) {
        this.entrusde = entrusde;
    }

    public MaiEnvios getEntrenvi() {
        return entrenvi;
    }

    public void setEntrenvi(MaiEnvios entrenvi) {
        this.entrenvi = entrenvi;
    }

    public MaiMensajes getEntrmens() {
        return entrmens;
    }

    public void setEntrmens(MaiMensajes entrmens) {
        this.entrmens = entrmens;
    }

    public MaiTablas getEntrcate() {
        return entrcate;
    }

    public void setEntrcate(MaiTablas entrcate) {
        this.entrcate = entrcate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (entrcons != null ? entrcons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MaiEntregas)) {
            return false;
        }
        MaiEntregas other = (MaiEntregas) object;
        if ((this.entrcons == null && other.entrcons != null) || (this.entrcons != null && !this.entrcons.equals(other.entrcons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.MaiEntregas[ entrcons=" + entrcons + " ]";
    }
    
}
