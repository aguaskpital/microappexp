/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "AEX_PERFIL", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "AexPerfil.findAll", query = "SELECT a FROM AexPerfil a")})
public class AexPerfil implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "PERFCONS", nullable = false, precision = 22, scale = 10)
    private BigDecimal perfcons;
    @Column(name = "PERFNOMB", length = 1000)
    private String perfnomb;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usroperf")
    private List<AexUserrole> aexUserroleList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "uspeperf")
    private List<AexUserperf> aexUserperfList;

    public AexPerfil() {
    }

    public AexPerfil(BigDecimal perfcons) {
        this.perfcons = perfcons;
    }

    public BigDecimal getPerfcons() {
        return perfcons;
    }

    public void setPerfcons(BigDecimal perfcons) {
        this.perfcons = perfcons;
    }

    public String getPerfnomb() {
        return perfnomb;
    }

    public void setPerfnomb(String perfnomb) {
        this.perfnomb = perfnomb;
    }

    public List<AexUserrole> getAexUserroleList() {
        return aexUserroleList;
    }

    public void setAexUserroleList(List<AexUserrole> aexUserroleList) {
        this.aexUserroleList = aexUserroleList;
    }

    public List<AexUserperf> getAexUserperfList() {
        return aexUserperfList;
    }

    public void setAexUserperfList(List<AexUserperf> aexUserperfList) {
        this.aexUserperfList = aexUserperfList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (perfcons != null ? perfcons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AexPerfil)) {
            return false;
        }
        AexPerfil other = (AexPerfil) object;
        if ((this.perfcons == null && other.perfcons != null) || (this.perfcons != null && !this.perfcons.equals(other.perfcons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.AexPerfil[ perfcons=" + perfcons + " ]";
    }
    
}
