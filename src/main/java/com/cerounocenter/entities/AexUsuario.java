/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "AEX_USUARIO", catalog = "", schema = "APPEXP", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"USUAMAIL"})})
@NamedQueries({
    @NamedQuery(name = "AexUsuario.findAll", query = "SELECT a FROM AexUsuario a")})
public class AexUsuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "USUACONS", nullable = false)
    private Long usuacons;
    @Basic(optional = false)
    @Column(name = "USUANOMB", nullable = false, length = 100)
    private String usuanomb;
    @Basic(optional = false)
    @Column(name = "USUAAPEL", nullable = false, length = 100)
    private String usuaapel;
    @Basic(optional = false)
    @Column(name = "USUAMAIL", nullable = false, length = 100)
    private String usuamail;
    @Basic(optional = false)
    @Column(name = "USUAPASS", nullable = false, length = 100)
    private String usuapass;
    @Basic(optional = false)
    @Column(name = "USUAESTA", nullable = false)
    private long usuaesta;
    @Basic(optional = false)
    @Column(name = "USUAFERE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date usuafere;
    @Basic(optional = false)
    @Column(name = "USUATIPO", nullable = false)
    private long usuatipo;
    @Column(name = "USUAENRE")
    private Long usuaenre;
    @Column(name = "USUAFEAC")
    @Temporal(TemporalType.TIMESTAMP)
    private Date usuafeac;
    @Column(name = "USUAFEUI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date usuafeui;
    @Column(name = "USUAFORM")
    private Long usuaform;
    @Column(name = "USUAULIP", length = 20)
    private String usuaulip;
    @Column(name = "USUATELE", length = 100)
    private String usuatele;
    @Column(name = "USUADOCU", length = 100)
    private String usuadocu;
    @Column(name = "USUATIDO", length = 100)
    private String usuatido;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "teleusua")
    private List<AemTelefono> aemTelefonoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "enviusre")
    private List<MaiEnvios> maiEnviosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "clieusua")
    private List<AexCliente> aexClienteList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mensuscr")
    private List<MaiMensajes> maiMensajesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "uspeusua")
    private List<AexUserperf> aexUserperfList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "entrusde")
    private List<MaiEntregas> maiEntregasList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "repousua")
    private List<AemReporte> aemReporteList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cupausua")
    private List<AexCupopago> aexCupopagoList;

    public AexUsuario() {
    }

    public AexUsuario(Long usuacons) {
        this.usuacons = usuacons;
    }

    public AexUsuario(Long usuacons, String usuanomb, String usuaapel, String usuamail, String usuapass, long usuaesta, Date usuafere, long usuatipo) {
        this.usuacons = usuacons;
        this.usuanomb = usuanomb;
        this.usuaapel = usuaapel;
        this.usuamail = usuamail;
        this.usuapass = usuapass;
        this.usuaesta = usuaesta;
        this.usuafere = usuafere;
        this.usuatipo = usuatipo;
    }

    public Long getUsuacons() {
        return usuacons;
    }

    public void setUsuacons(Long usuacons) {
        this.usuacons = usuacons;
    }

    public String getUsuanomb() {
        return usuanomb;
    }

    public void setUsuanomb(String usuanomb) {
        this.usuanomb = usuanomb;
    }

    public String getUsuaapel() {
        return usuaapel;
    }

    public void setUsuaapel(String usuaapel) {
        this.usuaapel = usuaapel;
    }

    public String getUsuamail() {
        return usuamail;
    }

    public void setUsuamail(String usuamail) {
        this.usuamail = usuamail;
    }

    public String getUsuapass() {
        return usuapass;
    }

    public void setUsuapass(String usuapass) {
        this.usuapass = usuapass;
    }

    public long getUsuaesta() {
        return usuaesta;
    }

    public void setUsuaesta(long usuaesta) {
        this.usuaesta = usuaesta;
    }

    public Date getUsuafere() {
        return usuafere;
    }

    public void setUsuafere(Date usuafere) {
        this.usuafere = usuafere;
    }

    public long getUsuatipo() {
        return usuatipo;
    }

    public void setUsuatipo(long usuatipo) {
        this.usuatipo = usuatipo;
    }

    public Long getUsuaenre() {
        return usuaenre;
    }

    public void setUsuaenre(Long usuaenre) {
        this.usuaenre = usuaenre;
    }

    public Date getUsuafeac() {
        return usuafeac;
    }

    public void setUsuafeac(Date usuafeac) {
        this.usuafeac = usuafeac;
    }

    public Date getUsuafeui() {
        return usuafeui;
    }

    public void setUsuafeui(Date usuafeui) {
        this.usuafeui = usuafeui;
    }

    public Long getUsuaform() {
        return usuaform;
    }

    public void setUsuaform(Long usuaform) {
        this.usuaform = usuaform;
    }

    public String getUsuaulip() {
        return usuaulip;
    }

    public void setUsuaulip(String usuaulip) {
        this.usuaulip = usuaulip;
    }

    public String getUsuatele() {
        return usuatele;
    }

    public void setUsuatele(String usuatele) {
        this.usuatele = usuatele;
    }

    public String getUsuadocu() {
        return usuadocu;
    }

    public void setUsuadocu(String usuadocu) {
        this.usuadocu = usuadocu;
    }

    public String getUsuatido() {
        return usuatido;
    }

    public void setUsuatido(String usuatido) {
        this.usuatido = usuatido;
    }

    public List<AemTelefono> getAemTelefonoList() {
        return aemTelefonoList;
    }

    public void setAemTelefonoList(List<AemTelefono> aemTelefonoList) {
        this.aemTelefonoList = aemTelefonoList;
    }

    public List<MaiEnvios> getMaiEnviosList() {
        return maiEnviosList;
    }

    public void setMaiEnviosList(List<MaiEnvios> maiEnviosList) {
        this.maiEnviosList = maiEnviosList;
    }

    public List<AexCliente> getAexClienteList() {
        return aexClienteList;
    }

    public void setAexClienteList(List<AexCliente> aexClienteList) {
        this.aexClienteList = aexClienteList;
    }

    public List<MaiMensajes> getMaiMensajesList() {
        return maiMensajesList;
    }

    public void setMaiMensajesList(List<MaiMensajes> maiMensajesList) {
        this.maiMensajesList = maiMensajesList;
    }

    public List<AexUserperf> getAexUserperfList() {
        return aexUserperfList;
    }

    public void setAexUserperfList(List<AexUserperf> aexUserperfList) {
        this.aexUserperfList = aexUserperfList;
    }

    public List<MaiEntregas> getMaiEntregasList() {
        return maiEntregasList;
    }

    public void setMaiEntregasList(List<MaiEntregas> maiEntregasList) {
        this.maiEntregasList = maiEntregasList;
    }

    public List<AemReporte> getAemReporteList() {
        return aemReporteList;
    }

    public void setAemReporteList(List<AemReporte> aemReporteList) {
        this.aemReporteList = aemReporteList;
    }

    public List<AexCupopago> getAexCupopagoList() {
        return aexCupopagoList;
    }

    public void setAexCupopagoList(List<AexCupopago> aexCupopagoList) {
        this.aexCupopagoList = aexCupopagoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuacons != null ? usuacons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AexUsuario)) {
            return false;
        }
        AexUsuario other = (AexUsuario) object;
        if ((this.usuacons == null && other.usuacons != null) || (this.usuacons != null && !this.usuacons.equals(other.usuacons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.AexUsuario[ usuacons=" + usuacons + " ]";
    }
    
}
