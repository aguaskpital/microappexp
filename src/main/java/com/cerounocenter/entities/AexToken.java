/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "AEX_TOKEN", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "AexToken.findAll", query = "SELECT a FROM AexToken a")})
public class AexToken implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "TOKECONS", nullable = false)
    private long tokecons;
    @Id
    @Basic(optional = false)
    @Column(name = "TOKEUSUA", nullable = false)
    private Long tokeusua;
    @Basic(optional = false)
    @Column(name = "TOKEIPIP", nullable = false, length = 50)
    private String tokeipip;
    @Basic(optional = false)
    @Lob
    @Column(name = "TOKETOKE", nullable = false)
    private String toketoke;
    @Basic(optional = false)
    @Column(name = "TOKEFEIN", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date tokefein;
    @Basic(optional = false)
    @Column(name = "TOKEESTA", nullable = false, length = 5)
    private String tokeesta;

    public AexToken() {
    }

    public AexToken(Long tokeusua) {
        this.tokeusua = tokeusua;
    }

    public AexToken(Long tokeusua, long tokecons, String tokeipip, String toketoke, Date tokefein, String tokeesta) {
        this.tokeusua = tokeusua;
        this.tokecons = tokecons;
        this.tokeipip = tokeipip;
        this.toketoke = toketoke;
        this.tokefein = tokefein;
        this.tokeesta = tokeesta;
    }

    public long getTokecons() {
        return tokecons;
    }

    public void setTokecons(long tokecons) {
        this.tokecons = tokecons;
    }

    public Long getTokeusua() {
        return tokeusua;
    }

    public void setTokeusua(Long tokeusua) {
        this.tokeusua = tokeusua;
    }

    public String getTokeipip() {
        return tokeipip;
    }

    public void setTokeipip(String tokeipip) {
        this.tokeipip = tokeipip;
    }

    public String getToketoke() {
        return toketoke;
    }

    public void setToketoke(String toketoke) {
        this.toketoke = toketoke;
    }

    public Date getTokefein() {
        return tokefein;
    }

    public void setTokefein(Date tokefein) {
        this.tokefein = tokefein;
    }

    public String getTokeesta() {
        return tokeesta;
    }

    public void setTokeesta(String tokeesta) {
        this.tokeesta = tokeesta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tokeusua != null ? tokeusua.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AexToken)) {
            return false;
        }
        AexToken other = (AexToken) object;
        if ((this.tokeusua == null && other.tokeusua != null) || (this.tokeusua != null && !this.tokeusua.equals(other.tokeusua))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.AexToken[ tokeusua=" + tokeusua + " ]";
    }
    
}
