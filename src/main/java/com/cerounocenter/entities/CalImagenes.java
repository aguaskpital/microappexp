/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "CAL_IMAGENES", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "CalImagenes.findAll", query = "SELECT c FROM CalImagenes c")})
public class CalImagenes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "IMAGCONS", nullable = false)
    private Long imagcons;
    @Basic(optional = false)
    @Column(name = "IMAGIMCO", nullable = false)
    private long imagimco;
    @Column(name = "IMAGANO")
    private Short imagano;
    @Column(name = "IMAGMES")
    private Short imagmes;
    @Column(name = "IMAGAUTO", length = 100)
    private String imagauto;
    @Column(name = "IMAGMENS", length = 500)
    private String imagmens;
    @Column(name = "IMAGTEMA", length = 100)
    private String imagtema;
    @Column(name = "IMAGUSCR")
    private Long imaguscr;
    @Column(name = "IMAGFECR")
    @Temporal(TemporalType.TIMESTAMP)
    private Date imagfecr;
    @Column(name = "IMAGESTA")
    private Short imagesta;

    public CalImagenes() {
    }

    public CalImagenes(Long imagcons) {
        this.imagcons = imagcons;
    }

    public CalImagenes(Long imagcons, long imagimco) {
        this.imagcons = imagcons;
        this.imagimco = imagimco;
    }

    public Long getImagcons() {
        return imagcons;
    }

    public void setImagcons(Long imagcons) {
        this.imagcons = imagcons;
    }

    public long getImagimco() {
        return imagimco;
    }

    public void setImagimco(long imagimco) {
        this.imagimco = imagimco;
    }

    public Short getImagano() {
        return imagano;
    }

    public void setImagano(Short imagano) {
        this.imagano = imagano;
    }

    public Short getImagmes() {
        return imagmes;
    }

    public void setImagmes(Short imagmes) {
        this.imagmes = imagmes;
    }

    public String getImagauto() {
        return imagauto;
    }

    public void setImagauto(String imagauto) {
        this.imagauto = imagauto;
    }

    public String getImagmens() {
        return imagmens;
    }

    public void setImagmens(String imagmens) {
        this.imagmens = imagmens;
    }

    public String getImagtema() {
        return imagtema;
    }

    public void setImagtema(String imagtema) {
        this.imagtema = imagtema;
    }

    public Long getImaguscr() {
        return imaguscr;
    }

    public void setImaguscr(Long imaguscr) {
        this.imaguscr = imaguscr;
    }

    public Date getImagfecr() {
        return imagfecr;
    }

    public void setImagfecr(Date imagfecr) {
        this.imagfecr = imagfecr;
    }

    public Short getImagesta() {
        return imagesta;
    }

    public void setImagesta(Short imagesta) {
        this.imagesta = imagesta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (imagcons != null ? imagcons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CalImagenes)) {
            return false;
        }
        CalImagenes other = (CalImagenes) object;
        if ((this.imagcons == null && other.imagcons != null) || (this.imagcons != null && !this.imagcons.equals(other.imagcons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.CalImagenes[ imagcons=" + imagcons + " ]";
    }
    
}
