/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "AEX_ROLEPERM", catalog = "", schema = "APPEXP")
@NamedQueries({
    @NamedQuery(name = "AexRoleperm.findAll", query = "SELECT a FROM AexRoleperm a")})
public class AexRoleperm implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ROPECONS", nullable = false, precision = 22, scale = 10)
    private BigDecimal ropecons;
    @Column(name = "ROPEDESC", length = 1000)
    private String ropedesc;
    @Column(name = "ROPETIPO", length = 1000)
    private String ropetipo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mororope")
    private List<AexModurope> aexModuropeList;

    public AexRoleperm() {
    }

    public AexRoleperm(BigDecimal ropecons) {
        this.ropecons = ropecons;
    }

    public BigDecimal getRopecons() {
        return ropecons;
    }

    public void setRopecons(BigDecimal ropecons) {
        this.ropecons = ropecons;
    }

    public String getRopedesc() {
        return ropedesc;
    }

    public void setRopedesc(String ropedesc) {
        this.ropedesc = ropedesc;
    }

    public String getRopetipo() {
        return ropetipo;
    }

    public void setRopetipo(String ropetipo) {
        this.ropetipo = ropetipo;
    }

    public List<AexModurope> getAexModuropeList() {
        return aexModuropeList;
    }

    public void setAexModuropeList(List<AexModurope> aexModuropeList) {
        this.aexModuropeList = aexModuropeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ropecons != null ? ropecons.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AexRoleperm)) {
            return false;
        }
        AexRoleperm other = (AexRoleperm) object;
        if ((this.ropecons == null && other.ropecons != null) || (this.ropecons != null && !this.ropecons.equals(other.ropecons))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entities.AexRoleperm[ ropecons=" + ropecons + " ]";
    }
    
}
