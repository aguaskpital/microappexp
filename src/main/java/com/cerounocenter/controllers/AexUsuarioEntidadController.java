package com.cerounocenter.controllers;

import com.cerounocenter.models.AexUsuarioEntidadModel;
import com.cerounocenter.models.RequestGeneric;
import com.cerounocenter.models.ResponseGeneric;
import com.cerounocenter.services.impl.AexUsuarioEntidadServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/usuario-entidad")
public class AexUsuarioEntidadController extends ParentController {

	private static final long serialVersionUID = 1L;
	@Autowired
	private AexUsuarioEntidadServiceImpl usuarioEntidadService;

	@PostMapping("/valida")
	@ResponseBody
	public ResponseGeneric<?> validarUsuarioEntidad(@RequestBody RequestGeneric request) {
		ResponseGeneric<AexUsuarioEntidadModel> rg = new ResponseGeneric<AexUsuarioEntidadModel>();
		AexUsuarioEntidadModel objeto = null;
		try {
			objeto = usuarioEntidadService.validarEntidad(request.getUsuario(), request.getPassword(), request.getIp());
		} catch (Exception e) {
			rg.setMensaje(e.getMessage());
			rg.setCodigoTx(HttpStatus.EXPECTATION_FAILED.value());
			return super.build(rg);
		}
		if (objeto != null) {
			rg.setModel(objeto);
			rg.setCodigoTx(HttpStatus.OK.value());
			rg.setMensaje("Validacion exitosa.");
			return super.build(rg);
		} else {
			rg.setCodigoTx(HttpStatus.NOT_FOUND.value());
			rg.setMensaje("Acceso Incorrecto.");
			return super.build(rg);
		}
	}

	@PostMapping("/test")
	public AexUsuarioEntidadModel test(@RequestBody RequestGeneric request) {
		AexUsuarioEntidadModel model = new AexUsuarioEntidadModel();
		model.setUsuausua(request.getUsuario());
		model.setUsuapass(request.getPassword());
		return model;
	}
	
	@PostMapping("/nombre")
	@ResponseBody
	public String nombre(@RequestBody RequestGeneric request) {
		return request.getUsuario();
	}

}
