package com.cerounocenter.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cerounocenter.models.Aex004EntidadModel;
import com.cerounocenter.models.Aex006EntidadModel;
import com.cerounocenter.models.ResponseGeneric;
import com.cerounocenter.services.impl.PagosEntidadRecaudoServiceImpl;

@RestController
@RequestMapping("/entidad-recaudo")
public class PagoEntidadRecaudoController extends ParentController {

	private static final long serialVersionUID = 1L;

	@Autowired
	private PagosEntidadRecaudoServiceImpl pagoEntidadRecaudoService;

	@GetMapping("/contrato/{contrato}")
	public ResponseEntity<?> buscarContrato(@PathVariable(required = true) Long contrato) {
		ResponseGeneric<Aex004EntidadModel> rg = new ResponseGeneric<Aex004EntidadModel>();
		try {
			Aex004EntidadModel result = pagoEntidadRecaudoService.buscarContrato(contrato);
			rg.setModel(result);
			return super.ok(rg);
		} catch (Exception e) {
			rg.setMensaje(e.getMessage());
			return super.error(rg);
		}
	}
	
	@GetMapping("/cupon/{cupon}")
	public ResponseEntity<?> buscarCupon(@PathVariable(required = true) Long cupon) {
		ResponseGeneric<Aex006EntidadModel> rg = new ResponseGeneric<Aex006EntidadModel>();
		try {
			Aex006EntidadModel result = pagoEntidadRecaudoService.buscarCupon(cupon);
			rg.setModel(result);
			return super.ok(rg);
		} catch (Exception e) {
			rg.setMensaje(e.getMessage());
			return super.error(rg);
		}
	}
	
	@GetMapping("/existe/{cupon}/{estado}")
	public ResponseEntity<?> existeCupon(@PathVariable(required = true) Long cupon, @PathVariable(required = true) String estado) {
		ResponseGeneric<Boolean> rg = new ResponseGeneric<Boolean>();
		try {
			Boolean result = pagoEntidadRecaudoService.existeCupon(cupon, estado);
			rg.setModel(result);
			return super.ok(rg);
		} catch (Exception e) {
			rg.setMensaje(e.getMessage());
			return super.error(rg);
		}
	}

}
