package com.cerounocenter.controllers;

import java.io.Serializable;
import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.cerounocenter.models.ResponseGeneric;


public class ParentController implements Serializable {
	
	private static final String MENSAJE_OK = "Transaccion exitosa.";
	private static final long serialVersionUID = 1L;

	protected ResponseEntity<?> ok(ResponseGeneric<?> rg) {
        rg.setFechaTx(new Date());
        rg.setCodigoTx(HttpStatus.OK.value());
        rg.setMensaje(MENSAJE_OK);
        return new ResponseEntity<>(rg,HttpStatus.OK);
    }

    protected ResponseGeneric<?> build(ResponseGeneric<?> rg) {
        rg.setFechaTx(new Date());
        return rg;
    }
	
	protected ResponseEntity<?> error(ResponseGeneric<?> rg) {
        rg.setFechaTx(new Date());
        rg.setCodigoTx(HttpStatus.EXPECTATION_FAILED.value());
        return new ResponseEntity<>(rg,HttpStatus.EXPECTATION_FAILED);
    }

}
