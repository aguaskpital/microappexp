package com.cerounocenter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroserviceAppexpApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceAppexpApplication.class, args);
	}

}
